import { Alias, defineConfig, loadEnv } from 'vite';
// import reactRefresh from '@vitejs/plugin-react-refresh';
import path from 'path';
import * as tsconfig from './tsconfig.paths.json';
import react from "@vitejs/plugin-react";

function readAliasFromTsConfig(): Alias[] {
  const pathReplaceRegex = new RegExp(/\/\*$/, '');
  return Object.entries(tsconfig.compilerOptions.paths).reduce(
    (aliases, [fromPaths, toPaths]) => {
      const find = fromPaths.replace(pathReplaceRegex, '');
      const toPath = toPaths[0].replace(pathReplaceRegex, '');
      const replacement = path.resolve(__dirname, toPath);
      aliases.push({ find, replacement });
      return aliases;
    },
    [] as Alias[]
  );
}

export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  return defineConfig({
    base: '/',
    plugins: [react()],
    resolve: {
      alias: readAliasFromTsConfig(),
    },
    define: {
      'process.env': process.env,
    },
    server: {
      port: Number(process.env.REACT_PORT),
      // proxy: {
      //   '/projects': 'http://localhost:3080',
      //   '/v1/dependencies': process.env.REACT_APP_PROXY_HOST,
      //   '/v1/auth/login': process.env.REACT_APP_PROXY_HOST,
      //   '/v1/wireframes': process.env.REACT_APP_PROXY_HOST,
      //   '/v1/projects': process.env.REACT_APP_PROXY_HOST,
      //   '/v1/users': process.env.REACT_APP_PROXY_HOST,
      // }
    },
  });
};
