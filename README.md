# Boilerplate for ReactJS

This project is using typescript as its core language

---

## Features / Configuration

List of tools that are already configured in this project includes

- Husky: Will run the linter and unit tests on every commit
- EsLint: Will find problems in the code and enforce the code style
- React Testing Library: React's implementation of "Testing Library" for unit testing
- Sentry: Will find problems in the source code
- Cypress: Will run the E2E tests
- env-cmd: Environments configuration

---

## Main packages

- Axios: Http requests / Http interceptors
- React-query: React hooks for make http requests
- React-hook-form: Helper hooks for working with forms
- React-router-dom: Routing functionality

---

## More information

It is recommended to read this boilerplates's official documentation

---

## Recommendations

### VS-Code Extensions

- Sonarlint
- ESLint
- Jest
