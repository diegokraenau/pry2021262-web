import React, { lazy, Suspense } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import {
  HashRouter,
  Redirect,
  Route,
  Switch,
  useHistory,
} from 'react-router-dom';

import { LoadingPage } from '@pages';
import Interceptors from './core/interceptors/Interceptors';
import { useEffect } from 'react';
import { useContext } from 'react';
import { ManageModals } from '@modals';
import { ChannelType, TopicType } from '@enums';
import { SCLoader, SCSessionExpired } from '@components';
import { Logger } from '@classes';
import { RoleGuard } from '@guards';
import { Auth } from '@classes';
import { AuthContext, MessageBusContext, ProjectProvider } from '@contexts';
import { ManageAlerts } from '@alerts';
import { validateToken } from '@commons';
import Guide from '@modules/project/pages/guide/Guide';
import { ToastContainer } from 'react-toastify';
import { showWelcomeAlert } from './core/alerts/welcome.alert';
import ErrorBoundaryLayout from './core/layouts/ErrorBoundary/ErrorBoundaryLayout';

const HomeLayout = lazy(() => import('./core/layouts/HomeLayout'));
const StatsLayout = lazy(() => import('./core/layouts/StatsLayout'));
// const NotFoundPage = lazy(() => import('@pages/NotFoundPage'));
const LoginLayout = lazy(() => import('./core/layouts/LoginLayout'));
const ProjectLayout = lazy(() => import('./core/layouts/ProjectLayout'));
const ProjectDetailLayout = lazy(
  () => import('./core/layouts/ProjectDetailLayout')
);
const AddWireframeLayout = lazy(
  () => import('./core/layouts/AddWireframeLayout')
);
const EditUserLayout = lazy(() => import('./core/layouts/EditUserLayout'));

const RecoverPasswordLayout = lazy(
  () => import('./core/layouts/RecoverPasswordLayout')
);

const ChangePasswordLayout = lazy(
  () => import('./core/layouts/ChangePasswordLayout')
);

const queryClient = new QueryClient();

const App = () => {
  /*Variables */
  const { messageBus } = useContext(MessageBusContext);
  const { setUser, user } = useContext(AuthContext);
  const loggerService = new Logger();
  const auth = new Auth();

  useEffect(() => {
    console.log('AppVersion v1.0');
    window.addEventListener('popstate', () => {
      if (window.location.hash === '#/' && auth.getToken() && validateToken()) {
        window.history.go(1);
      }
    });
  }, []);

  /*Manage state */

  useEffect(() => {
    if (messageBus) {
      loggerService.log('message-bus-service', messageBus);
    }
  }, [messageBus]);

  /*Functions */

  const renderMessageBus = () => {
    switch (messageBus) {
      case ChannelType.Modals:
        return <ManageModals></ManageModals>;

      default:
        break;
    }
  };

  const renderLoader = () => {
    if (
      messageBus?.channel === ChannelType.Global &&
      messageBus?.topic === TopicType.Loader
    ) {
      return (
        <SCLoader
          active={messageBus?.data.active}
          title={messageBus?.data.title}
        ></SCLoader>
      );
    }

    return;
  };

  const RenderNavigate = () => {
    const history = useHistory();

    useEffect(() => {
      //Manage validate token
      if (!user && auth.getToken() && validateToken()) {
        setUser(auth.decodeToken());
        showWelcomeAlert(
          auth.decodeToken().name,
          auth.decodeToken().lastName,
          auth.decodeToken().role
        );

        if (auth.decodeToken().role === 'ADMINISTRATOR') {
          history.push('/stats');
        } else {
          history.push('/projects');
        }
      }

      //Manage routing with message bus
      if (
        messageBus?.channel === ChannelType.Global &&
        messageBus.topic === TopicType.Routing
      ) {
        history.push(messageBus?.data.url);
      }
    }, [user]);

    return <></>;
  };

  return (
    <>
      <ErrorBoundaryLayout>
        <ProjectProvider>
          {renderLoader()}
          <Interceptors />
          {renderMessageBus()}
          <ManageModals></ManageModals>
          <ManageAlerts></ManageAlerts>
          <ToastContainer />
          <QueryClientProvider client={queryClient}>
            <HashRouter>
              {/* <HashRouter> */}
              <RenderNavigate></RenderNavigate>
              <Suspense fallback={<LoadingPage />}>
                <Switch>
                  <Route exact path="/" component={LoginLayout} />
                  <Route
                    exact
                    path="/recover-password"
                    component={RecoverPasswordLayout}
                  ></Route>
                  <Route
                    exact
                    path="/change-password/:id"
                    component={ChangePasswordLayout}
                  ></Route>
                  {/* <Route exact path="/404" component={NotFoundPage} /> */}
                  {/* <Route path="/projects" component={ProjectLayout} /> */}
                  <RoleGuard
                    path="/projects/:id"
                    component={ProjectDetailLayout}
                  ></RoleGuard>
                  <RoleGuard
                    path="/projects"
                    component={ProjectLayout}
                  ></RoleGuard>
                  <RoleGuard
                    path="/edit-profile/:id"
                    component={EditUserLayout}
                  ></RoleGuard>
                  {/* <RoleGuard path="/recover-password" component={RecoverPasswordLayout}></RoleGuard> */}
                  <RoleGuard path="/guide" component={Guide}></RoleGuard>
                  <RoleGuard
                    path="/add-wireframe/:id"
                    component={AddWireframeLayout}
                  ></RoleGuard>
                  <RoleGuard path="/home" component={HomeLayout}></RoleGuard>
                  <RoleGuard path="/stats" component={StatsLayout}></RoleGuard>
                  <Redirect to="/" />
                </Switch>
              </Suspense>
            </HashRouter>
            {/* </HashRouter> */}
            <SCSessionExpired></SCSessionExpired>
          </QueryClientProvider>
        </ProjectProvider>
      </ErrorBoundaryLayout>
    </>
  );
};

export default App;
