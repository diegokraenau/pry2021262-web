import React, { useState } from 'react';
import styles from './ChangeThemeModal.module.scss';
import { useContext } from 'react';
import { MessageBusContext } from '@contexts';
import { ModalAnimation } from '@animations';
import { SCButton } from '@components';
import { ColorPicker, useColor } from 'react-color-palette';
import { updateProject } from '@modules/project/commons/endpoints/project.endpoint';
import { ChannelType, TopicType } from '@enums';

const ChangeThemeModal = () => {
  /*Variables */
  const [loading, setLoading] = useState<boolean>(false);
  const { messageBus, setMessageBus } = useContext(MessageBusContext);
  const [color, setColor] = useColor(
    'hex',
    messageBus?.data.colorCode ? messageBus?.data.colorCode : '#0049BD'
  );

  /*Functions */

  const close = () => {
    setMessageBus(null);
  };

  const changeTheme = async () => {
    setLoading(true);
    await updateProject(messageBus?.data.projectId, {
      theme: color.hex,
    }).then((res) => {
      if (res) {
        messageBus?.data.setProjectSelected({
          ...messageBus?.data.projectSelected,
          theme: color.hex,
        });
        setLoading(false);
        setMessageBus({
          channel: ChannelType.Modals,
          topic: TopicType.Success,
          data: {
            title: 'Actualización exitosa',
            description: 'Se actualizo el tema correctamente.',
            click: () => {
              setMessageBus(null);
            },
            close: () => {
              setMessageBus(null);
            },
          },
        });
      }
    });
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div className={`${styles['container__content']}`}>
          <h1 className="text-center mb-20">Cambiar tema</h1>
          <div className="full-width flex flex-center">
            <ColorPicker
              width={430}
              height={70}
              color={color}
              onChange={setColor}
              //   hideHSV
            />
          </div>
          <div className="full-width flex flex-center">
            <SCButton
              loading={loading}
              text="Guardar"
              onClick={() => changeTheme()}
            ></SCButton>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() => close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export default ChangeThemeModal;
