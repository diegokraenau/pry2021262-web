import React, { useContext, useState } from 'react';
import styles from './DecisionModal.module.scss';
import { MessageBusContext } from '@contexts';
import { SCButton } from '@components';
import Question from '@img/question.svg';
import { ModalAnimation } from '@animations';

const DecisionModal = () => {
  const { messageBus, setMessageBus } = useContext(MessageBusContext);
  const [loading, setLoading] = useState<boolean>(false);

  function timeout(ms: any) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  const manageFunction = async (func: any) => {
    setLoading(true);
    await func();
    setLoading(false);
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles['container__content']} flex-column flex-y-center`}
        >
          <div className={styles['container__content-img']}>
            <img src={Question}></img>
          </div>
          <h1 className="text-center">{messageBus?.data.title}</h1>
          <p className="text-center mt-15">{messageBus?.data.description}</p>
          <div
            className={`${styles['container__content__buttons']} flex-space-between mt-15`}
          >
            <SCButton
              // loading={loading}
              text="Aceptar"
              loading={loading}
              onClick={() =>
                messageBus.data?.click
                  ? manageFunction(messageBus.data.click)
                  : setMessageBus(null)
              }
            ></SCButton>
            <SCButton
              // loading={loading}
              text="Cancelar"
              outline={true}
              onClick={() =>
                messageBus.data?.cancel
                  ? messageBus.data.cancel()
                  : setMessageBus(null)
              }
            ></SCButton>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() =>
              messageBus.data?.close
                ? messageBus.data.close()
                : setMessageBus(null)
            }
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export { DecisionModal };
