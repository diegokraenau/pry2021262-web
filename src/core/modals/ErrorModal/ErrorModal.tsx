import React, { useContext } from 'react';
import styles from './ErrorModal.module.scss';
import { MessageBusContext } from '@contexts';
import { SCButton } from '@components';
import Error from '@img/error.svg';
import { ModalAnimation } from '@animations';

const ErrorModal = () => {
  const { messageBus, setMessageBus } = useContext(MessageBusContext);

  const close = () => {
    setMessageBus(null);
  };

  //TODO:Ver esta lógica
  const accept = () => {
    setMessageBus(null);
    console.log('Debe eliminar el token o algo');
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles['container__content']} flex-wrap flex-x-center`}
        >
          <div className={styles['container__content-img']}>
            <img src={Error}></img>
          </div>
          <h1 className="text-center">{messageBus?.data.title}</h1>
          <p className="text-center">{messageBus?.data.description}</p>
          <div
            className={`${styles['container__content__buttons']} flex-center mt-15`}
          >
            <SCButton
              // loading={loading}
              text="Aceptar"
              onClick={() => accept()}
            ></SCButton>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() => close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export { ErrorModal };
