import React, { useState } from 'react';
import { SCButton, SCForm, SCInput } from '@components';
import styles from './RegisterModal.module.scss';
import { useContext } from 'react';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { ProjectContext, MessageBusContext, AuthContext } from '@contexts';
import { ModalAnimation } from '@animations';
import { createUser } from '../../../modules/user/pages/commons/endpoints/user.endpoint';
import { User } from '@interfaces';
import moment from 'moment';
import { ChannelType, TopicType } from '@enums';

const RegisterModal = () => {
  /*Variables */
  const [loading, setLoading] = useState<boolean>(false);
  const { setMessageBus } = useContext(MessageBusContext);

  const schema = yup.object().shape({
    name: yup
      .string()
      .required('Es necesario un nombre')
      .min(3, 'Mínimo 3 caracteres')
      .max(70, 'Máximo 70 caracteres'),
    lastName: yup
      .string()
      .required('Es necesario un apellido')
      .min(3, 'Mínimo 3 caracteres')
      .max(70, 'Máximo 70 caracteres'),
    email: yup
      .string()
      .email('Es necesario un correo')
      .required('Es necesario un correo')
      .min(10, 'Mínimo 10 caracteres')
      .max(100, 'Máximo 100 caracteres'),
    password: yup
      .string()
      .required('Es necesario una contraseña')
      .min(5, 'Mínimo 5 caracteres')
      .max(20, 'Máximo 20 caracteres'),
  });

  const { handleSubmit, register, formState, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  /*Functions */
  const addUser = async (data: any) => {
    data.created_at = moment(new Date()).format('YYYY/MM/DD');
    setLoading(true);
    const newUser: User | null = await createUser(data);
    setLoading(false);
    if (newUser) {
      close();
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Success,
        data: {
          title: 'Registro exitoso',
          description: 'Se creo una cuenta exitosamente.',
          click: () => {
            setMessageBus(null);
          },
          close: close(),
        },
      });
    }
    // setMessageBus(null);
  };

  const close = () => {
    setMessageBus(null);
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles['container__content']} flex-wrap flex-x-center`}
        >
          <h1 className="text-center">Registro</h1>
          <div className={`${styles['container__content__form']}`}>
            <SCForm
              handleSubmit={handleSubmit}
              register={register}
              formState={formState}
              onSubmit={addUser}
              control={control}
              className="flex flex-space-between"
            >
              <SCInput
                label="Nombres"
                name="name"
                placeholder="Ingrese los nombres"
                type="text"
                className="col-5"
              ></SCInput>
              <SCInput
                label="Apellidos"
                name="lastName"
                placeholder="Ingrese los apellidos"
                type="text"
                className="col-5"
              ></SCInput>
              <SCInput
                label="Email"
                name="email"
                placeholder="Ingrese su email"
                type="text"
                className="col-5"
              ></SCInput>
              <SCInput
                label="Contraseña"
                name="password"
                placeholder="Ingrese su contraseña"
                type="password"
                className="col-5"
              ></SCInput>
              <div
                className={`${styles['container__content__form-button']} flex flex-center mt-20`}
              >
                <SCButton
                  loading={loading}
                  text="Agregar"
                  disable={formState.isValid ? false : true}
                ></SCButton>
              </div>
            </SCForm>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() => close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export default RegisterModal;
