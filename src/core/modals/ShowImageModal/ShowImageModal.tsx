import React from 'react';
import styles from './ShowImageModal.module.scss';
import { useContext } from 'react';
import { MessageBusContext } from '@contexts';
import { ModalAnimation } from '@animations';

const ShowImageModal = () => {
  /*Variables */
  const { messageBus, setMessageBus } = useContext(MessageBusContext);

  /*Functions */
  const close = () => {
    setMessageBus(null);
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles['container__content']} flex-wrap flex-x-center`}
        >
          <div className={`${styles['container__content__image']}`}>
            <img src={messageBus?.data.url}></img>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() => close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export { ShowImageModal };
