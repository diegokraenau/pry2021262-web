import React, { useState } from 'react';
import { SCButton, SCForm, SCInput } from '@components';
import styles from './AddProjectModal.module.scss';
import { useContext } from 'react';
import * as yup from 'yup';
import { ChannelType, TopicType } from '@enums';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { createProject } from '@modules/project/commons/endpoints/project.endpoint';
import { ProjectContext, MessageBusContext, AuthContext } from '@contexts';
import { ModalAnimation } from '@animations';

const AddProjectModal = () => {
  /*Variables */
  const { user } = useContext(AuthContext);
  const [loading, setLoading] = useState<boolean>(false);
  const { setMessageBus } = useContext(MessageBusContext);
  const { projects, setProjects, setCopyProjects, copyProjects } =
    useContext(ProjectContext);

  const schema = yup.object().shape({
    name: yup
      .string()
      .required('Es necesario un nombre')
      .min(5, 'Mínimo 5 carácteres')
      .max(20, 'Máximo 20 caracteres'),
  });

  const { handleSubmit, register, formState, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  /*Functions */
  const addProject = async (data: any) => {
    setLoading(true);
    const validation = projects.filter(
      (project: { name: string }) =>
        project.name.toLowerCase() === data.name.toLowerCase()
    );
    if (validation.length !== 0) {
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Error,
        data: {
          title: 'Ya existe un proyecto con el mismo nombre.',
          description: 'Por favor, ingresé otro nombre.',
          click: () => setMessageBus(null),
          close: () => setMessageBus(null),
        },
      });
      return;
    }
    const newProject = await createProject(user?.id, data?.name);
    setLoading(false);
    setProjects([...projects, newProject]);
    // setCopyProjects([...copyProjects, newProject]);
    setMessageBus(null);
  };

  const close = () => {
    setMessageBus(null);
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles['container__content']} flex-wrap flex-x-center`}
        >
          <h1 className="text-center">Agregar proyecto</h1>
          <div className={`${styles['container__content__form']}`}>
            <SCForm
              handleSubmit={handleSubmit}
              register={register}
              formState={formState}
              onSubmit={addProject}
              control={control}
            >
              <SCInput
                label="Nombre"
                name="name"
                placeholder="Ingrese el nombre"
                type="text"
              ></SCInput>
              <div
                className={`${styles['container__content__form-button']} flex flex-center mt-10`}
              >
                <SCButton
                  loading={loading}
                  text="Agregar"
                  disable={formState.isValid ? false : true}
                ></SCButton>
              </div>
            </SCForm>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() => close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export default AddProjectModal;
