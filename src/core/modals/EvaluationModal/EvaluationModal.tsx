import React, { useContext, useState } from 'react';
import styles from './EvaluationModal.module.scss';
import { MessageBusContext } from '@contexts';
import { SCButton } from '@components';
import { ModalAnimation } from '@animations';

const EvaluationModal = () => {
  const { messageBus, setMessageBus } = useContext(MessageBusContext);
  const [loading, setLoading] = useState<boolean>(false);

  const close = () => {
    setMessageBus(null);
  };

  const accept = () => {
    setLoading(true);
    setTimeout(() => {
      console.log('Data', messageBus?.data);
      setLoading(false);
    }, 3000);
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles['container__content']} flex-wrap flex-x-center`}
        >
          <h1 className="text-center">¿Estas seguro de cerrar sesión?</h1>
          <p className="text-center">
            Mensaje de confirmación para cerrar sesión
          </p>
          <div
            className={`${styles['container__content__buttons']} flex-space-between`}
          >
            <SCButton
              loading={loading}
              text="Aceptar"
              onClick={() => accept()}
            ></SCButton>
            <SCButton
              text="Cancelar"
              outline={true}
              onClick={() => close()}
            ></SCButton>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() => close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export { EvaluationModal };
