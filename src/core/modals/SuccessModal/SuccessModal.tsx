import React, { useContext } from 'react';
import styles from './SuccessModal.module.scss';
import { MessageBusContext } from '@contexts';
import { SCButton } from '@components';
import Success from '@img/success.svg';
import { ModalAnimation } from '@animations';

const SuccessModal = () => {
  const { messageBus, setMessageBus } = useContext(MessageBusContext);

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles['container__content']} flex-column flex-y-center`}
        >
          <div className={styles['container__content-img']}>
            <img src={Success}></img>
          </div>
          <h1 className="text-center">{messageBus?.data.title}</h1>
          <p className="text-center mt-15">{messageBus?.data.description}</p>
          <div
            className={`${styles['container__content__buttons']} flex-center mt-15`}
          >
            <SCButton
              // loading={loading}
              text="Aceptar"
              onClick={() => messageBus?.data.click()}
            ></SCButton>
          </div>
          <span
            className={styles['container__content-close']}
            onClick={() => messageBus?.data.close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export { SuccessModal };
