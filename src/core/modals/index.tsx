import React from 'react';
import { TopicType } from '@enums';
import { EvaluationModal } from './EvaluationModal';
import { useContext, useEffect } from 'react';
import { MessageBusContext } from '@contexts';
import { ErrorModal } from './ErrorModal/ErrorModal';
import AddProjectModal from './AddProjectModal/AddProjectModal';
import { ShowImageModal } from './ShowImageModal/ShowImageModal';
import { SuccessModal } from './SuccessModal/SuccessModal';
import RegisterModal from './RegisterModal/RegisterModal';
import ChangeThemeModal from './ChangeThemeModal/ChangeThemeModal';
import { DecisionModal } from './DecisionModal/DecisionModal';

export const ManageModals = () => {
  const { messageBus } = useContext(MessageBusContext);

  const rendersModals = () => {
    switch (messageBus?.topic) {
      case TopicType.Evaluation:
        return <EvaluationModal></EvaluationModal>;
      case TopicType.AddProject:
        return <AddProjectModal></AddProjectModal>;
      case TopicType.Zoom:
        return <ShowImageModal></ShowImageModal>;
      case TopicType.Error:
        return <ErrorModal></ErrorModal>;
      case TopicType.Success:
        return <SuccessModal></SuccessModal>;
      case TopicType.RegisterModal:
        return <RegisterModal></RegisterModal>;
      case TopicType.ChanegThemeModal:
        return <ChangeThemeModal></ChangeThemeModal>;
      case TopicType.DecisionModal:
        return <DecisionModal></DecisionModal>
    }
  };

  return <>{rendersModals()}</>;
};
