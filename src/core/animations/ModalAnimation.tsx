import React from 'react';
import { motion } from 'framer-motion';

type Props = {
  children: any;
};

const ModalAnimation = ({ children }: Props) => {
  return (
    <motion.div
      initial={{ y: '-100vw' }}
      animate={{ y: 'calc(100vw - 100vw)' }}
      transition={{ delay: 0.2 }}
    >
      {children}
    </motion.div>
  );
};

export { ModalAnimation };
