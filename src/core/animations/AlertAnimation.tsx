import React from 'react';
import { AnimatePresence, motion } from 'framer-motion';

type Props = {
  children: any;
};

const AlertAnimation = ({ children }: Props) => {
  return (
    <AnimatePresence initial={true}>
      <motion.div
        positionTransition
        initial={{ y: -120 }}
        animate={{ y: 0 }}
        className="alert-container-animation"
        // exit={{ opacity: 0, scale: 1, transition: { duration: 0.2 } }}
      >
        {children}
      </motion.div>
    </AnimatePresence>
  );
};

export { AlertAnimation };
