import { toast } from 'react-toastify';

export const showSignoutAlert = () => {
  toast.dismiss();
  toast.error('Sesión finalizada.', {
    position: 'bottom-left',
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};
