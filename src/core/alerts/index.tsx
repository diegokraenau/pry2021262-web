import React from 'react';
import { TopicType } from '@enums';
import { useContext, useEffect } from 'react';
import { MessageBusContext } from '@contexts';
import { WarningAlert } from './warning-alert/WarningAlert';

export const ManageAlerts = () => {
  const { messageBus } = useContext(MessageBusContext);

  const renderAlerts = () => {
    switch (messageBus?.topic) {
      case TopicType.AlertWarning:
        return <WarningAlert></WarningAlert>;
    }
  };

  return <>{renderAlerts()}</>;
};

