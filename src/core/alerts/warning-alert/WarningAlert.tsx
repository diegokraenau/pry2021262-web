import React, { useContext } from 'react';
import style from './WarningAlert.module.scss';
import { MessageBusContext } from '@contexts';
import WarningImg from '@img/warning.svg';
import {AlertAnimation} from '@animations';

const WarningAlert = () => {
  /*Variables */
  const { messageBus, setMessageBus } = useContext(MessageBusContext);

  /*Functions */
  const close = () => {
    setMessageBus(null);
  };
  return (
    <AlertAnimation>
      <div className={style.container}>
        <div className={style.container__content}>
          <div className={style['container__content__logo']}>
            <img
              className={style['container__content__logo-icon']}
              src={WarningImg}
            ></img>
          </div>
          <div className={style['container__content__information']}>
            <h3 className={style['container__content__information-title']}>
              {messageBus?.data.title}
            </h3>
            <ul
              className={style['container__content__information-descriptions']}
            >
              {messageBus?.data.descriptions.map((x: string, i: any) => (
                <li key={i}>{x}</li>
              ))}
            </ul>
          </div>
          <div
            className={style['container__content__information-close']}
            onClick={() => close()}
          >
            &times;
          </div>
        </div>
      </div>
    </AlertAnimation>
  );
};

export { WarningAlert };
