import { toast } from 'react-toastify';

export const showGlobalWarningAlert = (msg: string) => {
  toast.dismiss();
  toast.warn(msg, {
    position: 'bottom-left',
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};
