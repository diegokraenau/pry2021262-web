import { toast } from 'react-toastify';

export const showWelcomeAlert = (
  name: string,
  lastName: string,
  userType: string
) => {
  toast.dismiss();
  toast.info(
    userType === 'ADMINISTRATOR'
      ? 'Bienvenido usuario Administrador.'
      : `Bienvenido ${name}, ${lastName}.`,
    {
      position: 'bottom-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }
  );
};
