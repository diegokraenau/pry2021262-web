import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const StadisticsDetailedPage = lazy(
    () => import('@modules/stats/pages/Stadistics')
);

const ProjectDetailLayout = () => {
    const { path } = useRouteMatch();

    return (
        <Suspense fallback={<LoadingPage></LoadingPage>}>
            <Switch>
                <Route exact path={path} component={StadisticsDetailedPage} />
            </Switch>
        </Suspense>
    );
};

export default ProjectDetailLayout;
