import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const ProjectPage = lazy(
  () => import('../../modules/project/pages/home-project/HomeProjectPage')
);

const ProjectLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route exact path={path} component={ProjectPage} />
      </Switch>
    </Suspense>
  );
};

export default ProjectLayout;
