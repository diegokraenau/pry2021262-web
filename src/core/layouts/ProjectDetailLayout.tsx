import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const ProjectDetailPage = lazy(
  () => import('@modules/project/pages/project-detail/ProjectDetail')
);

const ProjectDetailLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route exact path={path} component={ProjectDetailPage} />
      </Switch>
    </Suspense>
  );
};

export default ProjectDetailLayout;
