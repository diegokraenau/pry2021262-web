import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const RecoverPasswordPage = lazy(
  () => import('../../modules/auth/pages/recover-password/RecoverPasswordPage')
);

const RecoverPasswordLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route exact path={path} component={RecoverPasswordPage} />
      </Switch>
    </Suspense>
  );
};

export default RecoverPasswordLayout;
