import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const AddWireframePage = lazy(
  () => import('@modules/project/pages/add-wireframe/AddWireframe')
);

const AddWireframeLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route exact path={path} component={AddWireframePage} />
      </Switch>
    </Suspense>
  );
};

export default AddWireframeLayout;
