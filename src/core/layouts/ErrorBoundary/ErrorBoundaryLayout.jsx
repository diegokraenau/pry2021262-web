import React, { Component } from 'react'
import styles from './ErrorBoundaryLayout.module.scss'
import ErrorLogo from '@img/error_logo.svg'

class ErrorBoundaryLayout extends Component {
  constructor (props) {
    super(props)

    this.state = {
      hasError: false
    }
  }

  static getDerivedStateFromError (Error) {
    return {
      hasError: true
    }
  }

  render () {
    if (this.state.hasError) {
      return (
        <div className={styles.container}>
          <div className={styles.container__content}>
            <img src={ErrorLogo} alt='404'></img>
            <p>
              Ocurrio un error inesperado, por favor contactarse con
              <b> diegokraenau@gmail.com</b>. Muchas gracias.
            </p>
          </div>
        </div>
      )
    }
    return this.props.children
  }
}

export default ErrorBoundaryLayout
