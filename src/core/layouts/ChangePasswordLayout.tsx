import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const ChangePasswordPage = lazy(
  () => import('../../modules/auth/pages/change-password/ChangePasswordPage')
);

const ChangePasswordLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route exact path={path} component={ChangePasswordPage} />
      </Switch>
    </Suspense>
  );
};

export default ChangePasswordLayout;
