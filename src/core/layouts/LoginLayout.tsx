import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const LoginPage = lazy(
  () => import('@modules/auth/pages/login-page/LoginPage')
);

const LoginLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route exact path={path} component={LoginPage} />
      </Switch>
    </Suspense>
  );
};

export default LoginLayout;
