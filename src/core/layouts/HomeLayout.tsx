import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const HomePage = lazy(() => import('@modules/home/pages/HomePage'));

const HomeLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route path={path} component={HomePage} />
      </Switch>
    </Suspense>
  );
};

export default HomeLayout;
