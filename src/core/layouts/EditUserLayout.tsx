import React, { lazy, Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { LoadingPage } from '@pages';

const EditUserPage = lazy(
  () => import('@modules/user/pages/edit-user/EditUserPage')
);

const EditUserLayout = () => {
  const { path } = useRouteMatch();

  return (
    <Suspense fallback={<LoadingPage></LoadingPage>}>
      <Switch>
        <Route path={path} component={EditUserPage} />
      </Switch>
    </Suspense>
  );
};

export default EditUserLayout;
