import { Auth } from '../classes/Auth.class';
import jwt_decode from 'jwt-decode';

export const validateToken = (): boolean => {
  const auth = new Auth();
  const token: string | null = auth.getToken();
  if (token) {
    const user: any = jwt_decode(token);
    const exp = new Date(user?.exp * 1000);
    if (user?.id && new Date() <= exp) {
      // document
      //   .getElementById('session-expired-alert')
      //   ?.classList.add('display-none');
      return true;
    } else {
      // showSignoutAlert();
      // document
      //   .getElementById('session-expired-alert')
      //   ?.classList.remove('display-none');
      return false;
    }
  } else {
    return false;
  }
};
