const containsSpecialChars = (name: string) => {
  const specialChars = /[`!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?~]/;
  return specialChars.test(name);
};

const isCapitalLetter = (name: string, i: number) => {
  return name.charAt(i) === name.charAt(i).toUpperCase();
};

const containsCapitalLetter = (name: string) => {
  let found = false;
  for (let i = 0; i < name.length; i++) {
    if (isCapitalLetter(name, i)) {
      found = true;
    } else {
      found = false;
    }
  }
  return found;
};

export const validateCharactersWireframeName = (name: string): boolean => {
  //Validate that no contains a number
  if (name.match(/\d+/)) return false;

  //Validate that no contains a special character
  if (containsSpecialChars(name)) return false;

  return true;
};

export const convertToWireframeName = (name: string): string => {
  //Remove whitespace
  name = name.replace(/\s+/g, '');
  //Lower case first letter
  const firstLetter = name[0].toLocaleLowerCase();
  //Name without first letter
  let nameReplace = name.substring(1);
  //Validate after first letter
  nameReplace = nameReplace
    .replace(/([A-Z])/g, function ($1) {
      return `-${$1.toLocaleLowerCase()}`;
    })
    .trim();
  //Join
  return firstLetter + nameReplace;
};

export const validateWireframeName = (name: string) => {
  if (containsCapitalLetter(name)) return false;
  if (!validateCharactersWireframeName(name)) return false;

  return true;
};
