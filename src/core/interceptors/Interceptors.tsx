import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios';
// import { useHistory } from 'react-router-dom';
import { MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';

const Interceptors = () => {
  // History for redirection
  // const history = useHistory();

  const [authInterceptor, setAuthInterceptor] = useState<any>(undefined);
  const [errorInterceptor, setErrorInterceptor] = useState<any>(undefined);
  const { setMessageBus } = useContext(MessageBusContext);

  const addAuthInterceptor = () => {
    const authInterceptor = axios.interceptors.request.use(
      (request) => {
        const jwt = localStorage.getItem('token');

        if (jwt) {
          request.headers['Authorization'] = `Bearer ${jwt}`;
        }

        return request;
      },
      (error) => {
        return Promise.reject(error);
      }
    );
    setAuthInterceptor(authInterceptor);
  };

  const addErrorInterceptor = () => {
    const authInterceptor = axios.interceptors.response.use(
      (response) => {
        return response;
      },
      (error: any) => {
        if (error.response) {
          if (error.response.status === 401) {
            setMessageBus({
              channel: ChannelType.Modals,
              topic: TopicType.Error,
              data: {
                title: `Credenciales incorrectas`,
                description: 'Email o contraseña incorrectos.',
              },
            });
          } else {
            setMessageBus({
              channel: ChannelType.Modals,
              topic: TopicType.Error,
              data: {
                title: `Error : ${error.response.status}`,
                description:
                  'Ha ocurrido un error interno, por favor intentelo en unos minutos.',
              },
            });
          }
        }

        return Promise.reject(error);
      }
    );

    setAuthInterceptor(authInterceptor);
  };

  const removeAuthInterceptor = () => {
    axios.interceptors.request.eject(authInterceptor);
    setAuthInterceptor(undefined);
  };

  const removeErrorInterceptor = () => {
    axios.interceptors.response.eject(errorInterceptor);
    setErrorInterceptor(undefined);
  };

  useEffect(() => {
    addAuthInterceptor();
    addErrorInterceptor();

    return () => {
      removeAuthInterceptor();
      removeErrorInterceptor();
    };
  }, []);

  return <></>;
};

export default Interceptors;
