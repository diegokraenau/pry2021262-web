import decode from 'jwt-decode';

export class Auth {
  public setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  public getToken(): string | null {
    const token = localStorage.getItem('token');
    return token;
  }

  //TODO:Make a interface
  public decodeToken(): any {
    const token = localStorage.getItem('token');
    if (token) {
      const decoded = decode(token);
      return decoded;
    } else {
      return null;
    }
  }
}
