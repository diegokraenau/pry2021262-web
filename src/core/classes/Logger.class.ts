export class Logger {
  public log(title: string, objs: any[]) {
    console.log(`%c ${title}`, 'background: #222; color: #7FFF00', objs);
  }
}
