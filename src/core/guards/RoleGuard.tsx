import React, { useEffect } from 'react';
import { Route, useHistory } from 'react-router-dom';
import { SCNavbar } from '@components';
import { validateToken } from '@commons';
import { showSignoutAlert } from '../alerts/signout.alert';

type Props = {
  component: any;
  path: string;
};

const hasPermissions = () => {
  return validateToken();
};

const ManageRedirect = () => {
  const history = useHistory();
  useEffect(() => {
    history.push('/');
    showSignoutAlert();
  }, []);
  return <></>;
};

const RoleGuard = ({
  component: Component,
  path: direction,
  ...rest
}: Props) => {
  return (
    <Route
      exact
      path={direction}
      {...rest}
      render={(props) =>
        hasPermissions() ? (
          <>
            <SCNavbar></SCNavbar>
            <Component {...props} />
          </>
        ) : (
          <ManageRedirect></ManageRedirect>
          // <Redirect to="/" push />
        )
      }
    />
  );
};

export { RoleGuard };
