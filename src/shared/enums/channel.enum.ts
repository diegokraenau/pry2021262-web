enum ChannelType {
  Modals = 'Modals',
  Global = 'Global'
}

export { ChannelType };
