enum TopicType {
  Evaluation = 'Evaluation',
  Error = 'Error',
  Loader = 'Loader',
  Routing = 'Routing',
  AddProject = 'AddProject',
  Zoom = 'Zoom',
  AlertWarning = 'AlertWarning',
  Success = 'Success',
  RegisterModal = 'Register-Modal',
  ChanegThemeModal = 'Change-Theme-Modal',
  DecisionModal = 'Decision-Modal'
}

export { TopicType };
