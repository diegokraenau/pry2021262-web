import React from 'react';
import { useContext, useEffect } from 'react';
import { MessageBusContext } from '../contexts/MessageBusContext';
import { ChannelType, TopicType } from '@enums';

const LoadingPage = () => {
  const { setMessageBus } = useContext(MessageBusContext);

  useEffect(() => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Loader,
      data: {
        active: true,
        title: 'Cargando. . .',
      },
    });

    return () => {
      setMessageBus(null);
    };
  }, []);

  return <></>;
};

export { LoadingPage };
