import React, { useState } from 'react';
import style from './SCSearchBar.module.scss';
import SearchImg from '@img/search.svg';

interface IProps {
  originalElements: any;
  setCopyElements: any;
  placeHolder: string;
}

const SCSearchBar = ({
  originalElements,
  setCopyElements,
  placeHolder,
}: IProps) => {
  /*Variables */
  const [value, setValue] = useState<string>('');

  /*Functions */
  const manageValue = (e: any) => {
    setValue(e.target.value);
    if (e.target.value == '') {
      setCopyElements(originalElements);
    }
    setCopyElements(
      originalElements.filter((x: any) =>
        x.name.toLowerCase().includes(e.target.value.toLowerCase())
      )
    );
  };

  return (
    <div className={style.container}>
      <div className={style.container__logo}>
        <img className={style['container__logo-icon']} src={SearchImg}></img>
      </div>
      <input
        className={style.container__input}
        value={value}
        onChange={(e) => manageValue(e)}
        placeholder={placeHolder}
      ></input>
    </div>
  );
};

export { SCSearchBar };
