import React from 'react';
import styles from './SCInput.module.scss';

const SCInput = (props: any) => {
  // let containerStyle = styles.container;
  const { formState, register, name, label, placeholder, className, ...rest } =
    props;

  // if (className) {
  //   containerStyle = {
  //     ...styles.container,
  //     className,
  //   };
  // }

  return (
    <div className={`${styles.container} ${className && className}`}>
      <span className="label-input">{label}</span>
      <input
        autoComplete="off"
        name="name"
        className={`${styles.container__input} ${
          formState.errors[name] && 'border-error'
        }`}
        placeholder={placeholder}
        {...register(name)}
        {...rest}
      ></input>
      {formState.errors[name] && (
        <span className="error-message">*{formState.errors[name].message}</span>
      )}
    </div>
  );
};

export { SCInput };
