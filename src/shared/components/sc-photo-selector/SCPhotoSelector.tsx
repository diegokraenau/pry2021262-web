import React, { useEffect, useRef, useState } from 'react';
import style from './SCPhotoSelector.module.scss';
import UploadImage from '@img/upload_file.svg';
import { useContext } from 'react';
import { MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import { useFieldArray, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import AddImg from '@img/add.svg';
import {
  validateCharactersWireframeName,
  validateWireframeName,
  convertToWireframeName,
} from '@commons';

/*Globals */
const formSchema = {
  urlPreview: yup.string(),
  lastModified: yup.mixed(),
  lastModifiedDate: yup.mixed(),
  name: yup.string(),
  size: yup.mixed(),
  type: yup.string(),
  webkitRelativePath: yup.string(),
  nameView: yup
    .string()
    .test('test-name-view', 'Formato incorrecto', (value: any) => {
      const validation = validateWireframeName(value);
      return validation;
    })
    .min(5, 'Mínimo 5 letras')
    .required('Nombre requerido'),
};

const fieldsSchema = yup.object().shape({
  views: yup.array().of(yup.object().shape(formSchema)),
});

const SCPhotoSelector = (props: any) => {
  /*Variables */
  const { wireframes, setWireframes, setStepOneFormValid, files, setFiles } =
    props;
  const filesUploadButton = useRef(null);
  const moreFilesUploadButton = useRef(null);
  const { setMessageBus } = useContext(MessageBusContext);
  const { control, register, formState, handleSubmit, getValues } = useForm({
    mode: 'onChange',
    resolver: yupResolver(fieldsSchema),
  });
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'views',
  });

  /*Functions */
  const uploadFiles = () => {
    const button: any = filesUploadButton.current;
    if (button) {
      button.click();
    }
  };

  const handleFilesSelected = (e: any) => {
    const filesObject = e.target.files;
    const filesArray = [];
    for (let i = 0; i < filesObject?.length; i++) {
      const objectUrl = URL.createObjectURL(filesObject[i]);
      filesObject[i].urlPreview = objectUrl;
      filesArray.push(filesObject[i]);
      //To get file name without extension
      const fileName = filesObject[i].name.substr(
        0,
        filesObject[i].name.lastIndexOf('.')
      );

      append({
        urlPreview: filesObject[i].urlPreview,
        lastModified: filesObject[i].lastModified,
        lastModifiedDate: filesObject[i].lastModifiedDate,
        name: filesObject[i].name,
        size: filesObject[i].size,
        type: filesObject[i].type,
        webkitRelativePath: filesObject[i].webkitRelativePath,
        nameView: validateCharactersWireframeName(fileName)
          ? convertToWireframeName(fileName)
          : '',
      });
    }
    setFiles(filesArray);
  };

  const zoom = (url: string) => {
    setMessageBus({
      channel: ChannelType.Modals,
      topic: TopicType.Zoom,
      data: {
        url,
      },
    });
  };

  const onSubmit = (data: any) => console.log(data);

  const addMoreFiles = () => {
    const button: any = moreFilesUploadButton.current;
    if (button) {
      button.click();
    }
  };

  const handleMoreFiles = (e: any) => {
    const filesObject = e.target.files;
    const filesArray: any[] = [];
    for (let i = 0; i < filesObject?.length; i++) {
      const objectUrl = URL.createObjectURL(filesObject[i]);
      filesObject[i].urlPreview = objectUrl;
      filesArray.push(filesObject[i]);
      append({
        urlPreview: filesObject[i].urlPreview,
        lastModified: filesObject[i].lastModified,
        lastModifiedDate: filesObject[i].lastModifiedDate,
        name: filesObject[i].name,
        size: filesObject[i].size,
        type: filesObject[i].type,
        webkitRelativePath: filesObject[i].webkitRelativePath,
        nameView: '',
      });
    }
    //TODO:Fix
    setFiles((files: any[]) => files.concat(filesArray));
  };

  const removeFile = (i: any) => {
    remove(i);
    setFiles(files.filter((x: any, index: any) => index != i));
    setWireframes(getValues().views);
  };

  /*State management */
  useEffect(() => {
    if (formState.isValid && getValues().views.length > 0 && files) {
      setWireframes(getValues().views);
      setStepOneFormValid(true);
    } else {
      setStepOneFormValid(false);
    }
  }, [formState]);

  return (
    <div className={style.container}>
      <div className={style.container__content}>
        {fields.length == 0 ? (
          <div
            className={style.container__content__upload}
            onClick={() => uploadFiles()}
          >
            <img
              src={UploadImage}
              className={style['container__content__upload-img']}
            ></img>
            <h3 className="mt-10">Agregar imagenes</h3>
            <input
              ref={filesUploadButton}
              type="file"
              name="filefield"
              multiple
              className="invisible"
              onChange={handleFilesSelected}
              accept="image/png, image/jpeg"
            ></input>
          </div>
        ) : (
          <div className={style.container__content__files}>
            <form
              onSubmit={handleSubmit(onSubmit)}
              className={style['container__content__files__form']}
              autoComplete="off"
            >
              {fields.map((field: any, index: any) => (
                <div
                  className={style['container__content__files__item']}
                  key={index}
                >
                  <div
                    className={style['container__content__files__item__close']}
                    onClick={() => {
                      removeFile(index);
                    }}
                  >
                    <span
                      className={
                        style['container__content__files__item__close-icon']
                      }
                    >
                      &times;
                    </span>
                  </div>
                  <img
                    src={field.urlPreview}
                    onClick={() => zoom(field.urlPreview)}
                    className={style['container__content__files__item-img']}
                  ></img>
                  {/* <h3>{field.name}</h3> */}
                  <input
                    className={style['container__content__files__item-input']}
                    key={field.id}
                    {...register(`views.${index}.nameView` as any)}
                    placeholder="Ingrese el nombre"
                  ></input>
                  {formState.errors.views?.[index]?.nameView && (
                    <p
                      className={
                        style['container__content__files__item-input-error']
                      }
                    >
                      {formState.errors.views?.[index]?.nameView.message}
                    </p>
                  )}
                </div>
              ))}
              <div
                className={style['container__content__files__add']}
                onClick={() => {
                  addMoreFiles();
                }}
              >
                <img
                  src={AddImg}
                  className={style['container__content__files__add-img']}
                ></img>
                <h3 className="mt-10">Agregar</h3>
                <input
                  ref={moreFilesUploadButton}
                  type="file"
                  name="moreFileds"
                  multiple
                  className="invisible"
                  onChange={handleMoreFiles}
                  accept="image/png, image/jpeg"
                ></input>
              </div>
              {/* <input type="submit" /> */}
            </form>
          </div>
        )}
      </div>
    </div>
  );
};

export { SCPhotoSelector };
