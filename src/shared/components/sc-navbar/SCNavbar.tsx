import React, { useState } from 'react';
import style from './SCNavbar.module.scss';
import User from '@img/user.svg';
import { useContext } from 'react';
import { AuthContext, MessageBusContext } from '@contexts';
import { SCLoader } from '@components';
import { ChannelType, TopicType } from '@enums';
import Logo from '@img/logo.svg';
import SignoutImg from '@img/signout.svg';
import PersonImg from '@img/person.svg';
import { showSignoutAlert } from '@alerts/signout.alert';

const SCNavbar = () => {
  /*Variables */
  const { user, currentTab, setCurrentTab } = useContext(AuthContext);
  const { setMessageBus } = useContext(MessageBusContext);
  const [openOptions, setOpenOptions] = useState<boolean>(false);

  /*Functions */
  const changeTab = (tabIndex: number, url: string) => {
    setCurrentTab(tabIndex);
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Routing,
      data: {
        url,
      },
    });
    setOpenOptions(false);
  };

  const goToHome = () => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Routing,
      data: {
        url: '/projects',
      },
    });
  };

  const manageOptions = () => {
    setOpenOptions(!openOptions);
  };

  const backToLogin = () => {
    localStorage.clear();
    setOpenOptions(false);
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Routing,
      data: {
        url: '/',
      },
    });
    showSignoutAlert();
  };

  const editProfile = () => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Routing,
      data: {
        url: `/edit-profile/${user?.id}`,
      },
    });
    setOpenOptions(false);
    setCurrentTab(0);
  };

  /*Manage state */
  if (!user) {
    return <SCLoader active={true} title="Cargando . . ."></SCLoader>;
  }

  return (
    <div className={style.container}>
      <div
        className={`${style.container__content} flex flex-space-between  limit-container`}
      >
        <div className={style['container__content-logo']}>
          {/* <p>Logo</p> */}
          <img src={Logo} onClick={() => goToHome()}></img>
        </div>
        <div
          className={`${style.container__content__right} flex flex-y-center `}
        >
          <div className={style['container__content__right-options']}>
            <ul>
              {user?.role === 'ADMINISTRATOR' ? (
                <li
                  className={currentTab === 1 ? 'tab-activated' : ''}
                  onClick={() => changeTab(1, '/stats')}
                >
                  Estadisticas
                </li>
              ) : (
                <>
                  <li
                    className={currentTab === 1 ? 'tab-activated' : ''}
                    onClick={() => changeTab(1, '/projects')}
                  >
                    Crear proyecto
                  </li>
                  <li
                    className={currentTab === 2 ? 'tab-activated' : ''}
                    onClick={() => changeTab(2, '/guide')}
                  >
                    Guía
                  </li>
                </>
              )}
            </ul>
          </div>
          <div
            className={style['container__content__right-settings']}
            onClick={() => manageOptions()}
          >
            <img
              src={user.imgUrl}
              onError={({ currentTarget }) => {
                currentTarget.onerror = null; // prevents looping
                currentTarget.src = User;
              }}
            ></img>
          </div>
          {openOptions && (
            <div className={style['container__content__options-list']}>
              <div
                className={style['container__content__options-list-item']}
                onClick={() => editProfile()}
              >
                <img src={PersonImg}></img>
                <p>Mi perfil</p>
              </div>
              <div
                className={style['container__content__options-list-item']}
                onClick={() => backToLogin()}
              >
                <img src={SignoutImg}></img>
                <p>Cerrar sesión</p>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export { SCNavbar };
