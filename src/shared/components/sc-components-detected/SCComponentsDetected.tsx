import React from 'react';
import style from './SCComponentsDetected.module.scss';
import { GUI } from '../../interfaces/gui.interface';
import { Tag } from '@enums';
import BtnMock from '@img/btn_mock.svg';
import InputMock from '@img/input_mock.svg';
import RadioOnMock from '@img/radio_on_mock.svg';
import RadioOffMock from '@img/radio_off_mock.svg';
import CheckOffMock from '@img/check_off_mock.svg';
import CheckOnMock from '@img/check_on_mock.svg';
import ComboMock from '@img/combo_mock.svg';
import NumberMock from '@img/number_mock.svg';
import CardMock from '@img/card_mock.svg';
import CircleMock from '@img/circle_mock.svg';
import SquareMock from '@img/square_mock.svg';
import TextMock from '@img/text_mock.svg';
import NavbarMock from '@img/navbar_mock.svg';
import SliderMock from '@img/slider_mock.svg';
import TextAreaMock from '@img/text_area_mock.svg';
import shortid from 'shortid';

interface props {
  guis?: GUI[];
}

const SCComponentsDetected = ({ guis }: props) => {
  /*Variables */

  /*Functions */
  const manageSvgs = (tag: string) => {
    switch (tag) {
      case Tag.BUTTON:
        return <img src={BtnMock} key={shortid.generate()}></img>;
      case Tag.CARD:
        return <img src={CardMock} key={shortid.generate()}></img>;
      case Tag.CHECK_BOX_OFF:
        return <img src={CheckOffMock} key={shortid.generate()}></img>;
      case Tag.CHECK_BOX_ON:
        return <img src={CheckOnMock} key={shortid.generate()}></img>;
      case Tag.CIRCLE_IMAGE:
        return <img src={CircleMock} key={shortid.generate()}></img>;
      case Tag.COMBO_BOX:
        return <img src={ComboMock} key={shortid.generate()}></img>;
      case Tag.IMAGE:
        return <img src={SquareMock} key={shortid.generate()}></img>;
      case Tag.INPUT:
        return <img src={InputMock} key={shortid.generate()}></img>;
      case Tag.LABEL:
        return <img src={TextMock} key={shortid.generate()}></img>;
      case Tag.NAV_BAR:
        return <img src={NavbarMock} key={shortid.generate()}></img>;
      case Tag.NUMBER_INPUT:
        return <img src={NumberMock} key={shortid.generate()}></img>;
      case Tag.RADIO_BUTTON_OFF:
        return <img src={RadioOffMock} key={shortid.generate()}></img>;
      case Tag.RADIO_BUTTON_ON:
        return <img src={RadioOnMock} key={shortid.generate()}></img>;
      case Tag.SLIDER:
        return <img src={SliderMock} key={shortid.generate()}></img>;
      case Tag.TEXT_AREA:
        return <img src={TextAreaMock} key={shortid.generate()}></img>;

      default:
        return <p>Not found</p>;
    }
  };

  return (
    <div className={style.container}>
      <div className={style.container__content}>
        {guis &&
          guis.length > 0 &&
          guis.map((x, i) => (
            <div className={style.container__content__view} key={i}>
              <p className={style['container__content__view-title']}>
                Vista: {x.viewName}
              </p>
              <div className={style.container__content__view__predictions}>
                <p>Componentes: </p>
                {x.uniqueTags && x.uniqueTags.map((p, i) => manageSvgs(p))}
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export { SCComponentsDetected };
