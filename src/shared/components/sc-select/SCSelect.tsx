import React, { useState } from 'react';
import { Controller } from 'react-hook-form';
import style from './SCSelect.module.scss';

const SCSelect = (props: any) => {
  const [show, setShow] = useState<boolean>(false);
  const { label, name, items, formState, control, placeholder } = props;
  const [option, setOption] = useState<any>();

  const changeValue = (x: any, field: any) => {
    field.onChange(x?.name);
    setOption(x);
    setShow(!show);
  };

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <div className={style.container}>
          <span className="label-input">{label}</span>
          <input
            readOnly
            className={style.container__select}
            placeholder={placeholder}
            onClick={() => {
              setShow(!show);
            }}
            value={option?.name || ''}
          ></input>
          <div className="triangle"></div>
          <div className={`${style.container__content} ${!show && 'hide'}`}>
            {items?.length &&
              items.map((x: any) => (
                <div
                  key={x.id}
                  className={style['container__content--option']}
                  onClick={() => {
                    changeValue(x, field);
                  }}
                >
                  {x.name}
                </div>
              ))}
          </div>
          {formState.errors[name] && (
            <span className="error-message">
              *{formState.errors[name].message}
            </span>
          )}
        </div>
      )}
    />
  );
};

export { SCSelect };
