import React from 'react';
import style from './SCTabs.module.scss';

interface IProps {
  elements: any[];
  currentTab: number;
}

const SCTabs = ({ elements, currentTab }: IProps) => {
  return (
    <div className={style.container}>
      <div className={style.container__content}>
        {elements.map((x) => (
          <div
            className={`${style.container__content__item} ${
              currentTab != x.id && 'disable-tab'
            }`}
            onClick={() => x.click()}
            key={x.name}
          >
            <p className={`${style['container__content__item-text']}`}>
              {x.name}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
};

export { SCTabs };
