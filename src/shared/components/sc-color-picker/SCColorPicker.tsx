import React from 'react';
import { ColorPicker, useColor } from 'react-color-palette';
import 'react-color-palette/lib/css/styles.css';
import style from './SCColorPicker.module.scss';

interface props {
  color: any;
  setColor: any;
}

const SCColorPicker = ({ color, setColor }: props) => {
  return (
    <div className={style.container}>
      <div className={style.container__content}>
        <ColorPicker
          width={456}
          height={228}
          color={color}
          onChange={setColor}
          //   hideHSV
        />
      </div>
    </div>
  );
};

export { SCColorPicker };
