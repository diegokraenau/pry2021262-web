import React from 'react';
import styles from './SCButton.module.scss';

const SCButton = (props: any) => {
  const { text, loading, action, disable, outline, type, ...rest } = props;

  return (
    <div className={styles.container}>
      <button
        className={
          disable
            ? styles['container__button--disable']
            : outline
            ? styles['container__button--outline']
            : styles['container__button']
        }
        disabled={disable ? true : false}
        {...rest}
        type={type}
      >
        {loading ? (
          <div
            className={`${styles.container__loader} rotate ${
              outline && 'color-loader'
            }`}
          ></div>
        ) : (
          text
        )}
      </button>
    </div>
  );
};

export { SCButton };
