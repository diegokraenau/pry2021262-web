import React from 'react';
import style from './SCStepper.module.scss';

const SCStepper = (props: any) => {
  /*Variables */
  const { steps, currentStep } = props;

  return (
    <div className={style.container}>
      <ol className={style.container__content}>
        {steps.map((x: any, i: number) => (
          <li className={`${style.container__content__item}`} key={i}>
            <div
              className={`${style['container__content__item-circle']} 
                ${currentStep != i + 1 && 'disable-stepper'}`}
            >
              <h3>{i + 1}</h3>
            </div>
            <p className={style['container__content__item-desc']}>{x.name}</p>
          </li>
        ))}
      </ol>
    </div>
  );
};

export { SCStepper };
