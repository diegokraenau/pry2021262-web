import React from 'react';
import style from './SCSessionExpired.module.scss';
import { SCFont } from '@components';
import SessionExpiredSvg from '@img/session-expired.svg';
// import { ModalAnimation } from '@animations';
import shortid from 'shortid';

const SCSessionExpired = () => {
  const close = () => {
    document
      .getElementById('session-expired-alert')
      ?.classList.add('display-none');
  };

  return (
    <div
      id="session-expired-alert"
      className={`${style.content} easy-in-out-v2-animation display-none`}
      key={shortid.generate()}
    >
      <div className={style.content__information} key={shortid.generate()}>
        <SCFont
          text={[
            'Sesión ',
            <strong key={shortid.generate()}>Terminada</strong>,
          ]}
          type="subtitle"
          color="white"
        ></SCFont>
        <SCFont
          key={shortid.generate()}
          text="Ocurrió un error al refrescar tu sesión ingresa otra vez."
          color="white"
        ></SCFont>
      </div>
      <div>
        <img src={SessionExpiredSvg}></img>
      </div>
      <span className={style.content__close} onClick={() => close()}>
        &times;
      </span>
    </div>
  );
};

export { SCSessionExpired };
