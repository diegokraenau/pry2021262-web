import React from 'react';
import style from './SCForm.module.scss';

const SCForm = (props: any) => {
  const {
    formState,
    register,
    handleSubmit,
    children,
    onSubmit,
    control,
    className,
  } = props;
  // const { handleSubmit, handleSubmit } = useForm({ defaultValues });

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      autoComplete="off"
      className={`${style.container} ${className && className}`}
    >
      {Array.isArray(children)
        ? children.map((child) => {
            return child.props.name
              ? React.createElement(child.type, {
                  ...{
                    ...child.props,
                    register,
                    formState,
                    control,
                    key: child.props.name,
                  },
                })
              : child;
          })
        : children}
    </form>
  );
};

export { SCForm };
