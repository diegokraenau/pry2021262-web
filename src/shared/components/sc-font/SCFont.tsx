import React from 'react';
import styled from 'styled-components';
import './SCFont.module.scss';

interface IProps {
  text?: any;
  color?: string;
  type?: 'title' | 'subtitle' | 'label';
}

const manageType = (type: any) => {
  switch (type) {
    case 'subtitle':
      return { 'font-size': '18px' };
  }
};

const Font = styled.p<any>`
  color: ${(props: any) => (props.color ? props.color : 'black')};
  margin: 5px 0px;
  ${(props: any) => props.extras && props.extras}
`;

const SCFont = ({ text, color, type, ...rest }: IProps) => {
  return (
    <Font color={color} extras={manageType(type)} {...rest}>
      {text}
    </Font>
  );
};

export { SCFont };
