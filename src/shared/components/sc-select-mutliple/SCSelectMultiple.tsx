import React from 'react';
import style from './SCSelectMultiple.module.scss';

interface IProps {
  options: any[] | undefined;
  direction: 'vertically' | 'horizontal';
  updateSelectedArray: any;
}

const SCSelectMultiple = ({
  options,
  direction,
  updateSelectedArray,
}: IProps) => {
  /*Functions */
  const manageClick = (x: any, e: any) => {
    updateSelectedArray((arr: any[]) => {
      if (arr.find((y: any) => y == x)) {
        return arr.filter((z) => z != x);
      } else {
        return [...arr, x];
      }
    });
  };

  return (
    <div className={style.container}>
      <div
        className={`${style.container__content} ${
          direction == 'vertically' ? 'flow-vertically' : 'flow-horizontal'
        }`}
      >
        {options! && options.length > 0 ? (
          options.map((x: any) => (
            <div className={style.container__content__item} key={x.name}>
              <input
                type="checkbox"
                name="option"
                value={x.name}
                onClick={(e: any) => manageClick(x, e)}
              ></input>
              <p>{x.name}</p>
            </div>
          ))
        ) : (
          <p>No hay opciones.</p>
        )}
      </div>
    </div>
  );
};

export { SCSelectMultiple };
