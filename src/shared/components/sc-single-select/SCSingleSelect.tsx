import React from 'react';
import style from './SCSingleSelect.module.scss';

interface IProps {
  options: any[];
}

const SCSingleSelect = ({ options }: IProps) => {
  return (
    <div className={style.container}>
      <div className={style.container__content}>
        {options.map((x) => (
          <div className={style.container__content__item} key={x.name}>
            <input
              type="radio"
              name="option"
              value={x.name}
              onClick={x.click}
              defaultChecked={x.name === 'Angular' ? true : false}
              // checked={x.name === 'Angular' ? true : false}
            ></input>
            <p>{x.name}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export { SCSingleSelect };
