import React, { useState } from 'react';
import { GUI } from '@interfaces';
import style from './SCPreview.module.scss';
import BackIcon from '@img/back-carrousel.svg';
import NextIcon from '@img/next-carrousel.svg';
// import * as responseDecoded from '@mocks/guis-decoded.json';

interface props {
  guis?: GUI[];
}

const SCPreview = ({ guis }: props) => {
  /*Variables */
  const [order, setOrder] = useState<number>(0);
  const [animation, setAnimation] = useState<boolean>(false);

  /*Functions*/
  const manageHtml = () => {
    return guis![order].template_decoded;
  };

  const next = () => {
    setAnimation(true);
    if (order == guis!.length - 1) {
      setOrder(0);
    } else {
      setOrder((order) => order + 1);
    }
  };

  const back = () => {
    setAnimation(true);
    if (order == 0) {
      setOrder(guis!.length - 1);
    } else {
      setOrder((order) => order - 1);
    }
  };

  return (
    <div className={style.container}>
      <div className={style.container__content}>
        <div className={style.container__content__back}>
          <img
            className={style['container__content__back-icon']}
            src={BackIcon}
            onClick={() => {
              back();
            }}
          ></img>
        </div>
        <p className={style['container__content-title']}>
          Vista N° {order + 1} : {guis && guis[order].viewName}
        </p>

        <div
          className={`${style.container__content__preview} ${
            animation ? 'fade-in-image' : ''
          }`}
          onAnimationEnd={() => setAnimation(false)}
        >
          <iframe
            className={`${style['container__content__preview-photo']}`}
            srcDoc={manageHtml()}
          ></iframe>
        </div>
        <div className={style.container__content__next}>
          <img
            className={style['container__content__next-icon']}
            src={NextIcon}
            onClick={() => {
              next();
            }}
          ></img>
        </div>
      </div>
    </div>
  );
};

export { SCPreview };
