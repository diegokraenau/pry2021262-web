export interface Framework {
  id?: string;
  framework?: string;
  dependencies?: any[];
}
