export interface User {
  id?: string;
  email?: string;
  password?: string;
  role?: string;
  imgUrl?: string;
  created_at?: string;
  name?: string;
  lastName?: string;
}
