import { Tag } from 'shared/enums';

export interface Prediction {
  probability: number;
  tagName: Tag;
}
