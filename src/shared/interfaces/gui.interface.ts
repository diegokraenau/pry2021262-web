import { Tag } from 'shared/enums';
import { Prediction } from './prediction.interface';
export interface GUI {
  viewName?: string;
  template?: any;
  template_decoded?: any;
  predictions?: Prediction[];
  uniqueTags?: string[];
}
