export interface Project {
  id?: string;
  name?: string;
  created_date?: Date;
  views?: any[];
  theme?: string;
}
