export interface View {
  id?: string;
  name?: string;
  content?: string;
  template?: string;
  created_at?: Date;
  project_id?: string;
  wireframe_id?: string;
}
