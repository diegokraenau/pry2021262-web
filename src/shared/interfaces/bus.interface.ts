import { ChannelType, TopicType } from 'shared/enums';

export interface BusEntity {
  channel?: ChannelType;
  topic?: TopicType;
  data?: any;
}
