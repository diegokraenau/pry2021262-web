import React from 'react';
import { createContext } from 'react';
import { useState } from 'react';
import { User } from '@interfaces';

type Props = {
  children: any;
};

export const AuthContext = createContext<any | null>(null);

export const AuthProvider = ({ children }: Props) => {
  const [user, setUser] = useState<User | null>(null);
  const [currentTab, setCurrentTab] = useState<number>(1);

  return (
    <AuthContext.Provider value={{ user, setUser, currentTab, setCurrentTab }}>
      {children}
    </AuthContext.Provider>
  );
};
