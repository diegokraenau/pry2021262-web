import React from 'react';
import { createContext } from 'react';
import { useState } from 'react';
import { BusEntity } from '../interfaces/bus.interface';

type Props = {
  children: any;
};

export const MessageBusContext = createContext<any | null>(null);

export const MessageBusProvider = ({ children }: Props) => {
  const [messageBus, setMessageBus] = useState<BusEntity | null>();

  return (
    <MessageBusContext.Provider value={{ messageBus, setMessageBus }}>
      {children}
    </MessageBusContext.Provider>
  );
};
