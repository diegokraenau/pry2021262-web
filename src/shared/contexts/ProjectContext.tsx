import React from 'react';
import { createContext } from 'react';
import { useState } from 'react';
import { User } from '@interfaces';
import { Project } from '@interfaces';

type Props = {
  children: any;
};

export const ProjectContext = createContext<any | null>(null);

export const ProjectProvider = ({ children }: Props) => {
  const [projects, setProjects] = useState<Project[]>([]);
  const [copyProjects, setCopyProjects] = useState<Project[]>([]);

  return (
    <ProjectContext.Provider
      value={{ projects, setProjects, copyProjects, setCopyProjects }}
    >
      {children}
    </ProjectContext.Provider>
  );
};
