import React from 'react';
import ReactDOM from 'react-dom';
import 'react-toastify/dist/ReactToastify.css';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';

// import * as Sentry from '@sentry/react';
// import { Integrations } from '@sentry/tracing';
// import { environment } from './environments/environment';
import { MessageBusProvider, AuthProvider } from '@contexts';

// Sentry.init({
//   dsn: environment.SENTRY_DNS,
//   integrations: [new Integrations.BrowserTracing()],
//   tracesSampleRate: 1.0,
// });

ReactDOM.render(
  <React.StrictMode>
    <AuthProvider>
      <MessageBusProvider>
        <App />
      </MessageBusProvider>
    </AuthProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
