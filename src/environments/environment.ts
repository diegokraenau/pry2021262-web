export const environment = {
  APP_NAME: process.env.REACT_APP_NAME,
  APP_URL: process.env.REACT_APP_URL,
  SENTRY_DNS: process.env.SENTRY_DNS
};
