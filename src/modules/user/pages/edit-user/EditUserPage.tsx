import React, { useState, useEffect, useContext, useRef } from 'react';
import styles from './EditUser.module.scss';
import { useParams } from 'react-router-dom';
import { SCButton, SCForm, SCInput } from '@components';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { User } from '@interfaces';
import { getUserById, editUser } from '../commons/endpoints/user.endpoint';
import { AuthContext, MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import Edit from '@img/edit.svg';
import { updateProfilePicture } from '@modules/project/commons/endpoints/project.endpoint';
import { userInfo } from 'os';
import { Auth } from '@classes';
import JWT from 'expo-jwt';
import UserImg from '@img/user.svg';

const EditUserPage = () => {
  /*Variables */
  const { id } = useParams<any>();
  const [profile, setProfile] = useState<User | null>(null);
  const fileUploadButton = useRef(null);
  const { setMessageBus } = useContext(MessageBusContext);
  const [loading, setLoading] = useState<boolean>(false);
  const { user, setUser } = useContext(AuthContext);
  const { setCurrentTab } = useContext(AuthContext);
  const [files, setFiles] = useState<any>(null);
  const schema = yup.object().shape({
    name: yup
      .string()
      .required('Es necesario un nombre')
      .min(3, 'Mínimo 3 caracteres')
      .max(70, 'Máximo 70 caracteres'),
    lastName: yup
      .string()
      .required('Es necesario un apellido')
      .min(3, 'Mínimo 3 caracteres')
      .max(70, 'Máximo 70 caracteres'),
    email: yup
      .string()
      .email('Es necesario un correo')
      .required('Es necesario un correo')
      .min(10, 'Mínimo 10 caracteres')
      .max(100, 'Máximo 100 caracteres'),
    password: yup
      .string()
      .required('Es necesario una contraseña')
      .min(5, 'Mínimo 5 caracteres')
      .max(20, 'Máximo 20 caracteres'),
  });
  const key = 'mysecretkey';
  const { handleSubmit, register, formState, setValue, control, getValues } =
    useForm({
      mode: 'onChange',
      resolver: yupResolver(schema),
    });

  /*State management */
  useEffect(() => {
    if (id) {
      setCurrentTab(0);
      setMessageBus({
        channel: ChannelType.Global,
        topic: TopicType.Loader,
        data: {
          title: 'Cargando información...',
          active: true,
        },
      });
      getUserById(id)
        .then((res: User | null) => {
          if (res) {
            setProfile(res);
            setValue('name', res?.name);
            setValue('lastName', res?.lastName);
            setValue('password', res?.password);
            setValue('email', res?.email);
            setUser(res);
          }
        })
        .catch(() => {
          setMessageBus({
            channel: ChannelType.Global,
            topic: TopicType.Routing,
            data: {
              url: '/projects',
            },
          });
        })
        .finally(() => setMessageBus(null));
    }
  }, []);

  /*Functions */
  const editProfileAction = async (data: any) => {
    setLoading(true);
    await editUser(id, data)
      .then((res: User | null) => {
        if (res) {
          setValue('name', res.name);
          setValue('lastName', res?.lastName);
          setValue('password', res?.password);
          setValue('email', res?.email);
          setUser(res);
          setMessageBus({
            channel: ChannelType.Modals,
            topic: TopicType.Success,
            data: {
              title: 'Modificación exitosa',
              description: 'Se actualizo los datos correctamente.',
              click: () => {
                setMessageBus(null);
              },
              close: () => {
                setMessageBus(null);
              },
            },
          });
        }
      })
      .finally(() => setLoading(false));
  };

  const uploadFile = () => {
    const button: any = fileUploadButton.current;
    if (button) {
      button.click();
    }
  };

  const handleNewToken = (url: string) => {
    const auth = new Auth();
    const userDecoded = auth.decodeToken();
    userDecoded.imgUrl = url;
    const newToken = JWT.encode(userDecoded, key);
    localStorage.setItem('token', newToken);
  };

  const handleFilesSelected = async (e: any) => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Loader,
      data: {
        active: true,
        title: 'Actualizando perfil. . .',
      },
    });

    const fileObject = e.target.files[0];
    const objectUrl = URL.createObjectURL(fileObject);
    fileObject.urlPreview = objectUrl;
    const formData = new FormData();
    formData.append('file', fileObject, fileObject.name);

    await updateProfilePicture(profile?.id ? profile?.id : '', formData)
      .then((res: any) => {
        setProfile({
          ...profile,
          imgUrl: res?.url ? res?.url : profile?.imgUrl,
        });
        setUser({ ...user, imgUrl: res?.url ? res?.url : profile?.imgUrl });
        handleNewToken(res?.url);
      })
      .finally(() => setMessageBus(null));
  };

  return (
    <>
      {profile && (
        <div className={`${styles.container} limit-container`}>
          <div className={styles.container__content}>
            <h1 className="mb-20">Editar perfil</h1>
            <div className={styles.container__content__information}>
              <div className={styles.container__content__information__left}>
                <img
                  className={
                    styles.container__content__information__left__profile
                  }
                  src={profile.imgUrl}
                  onError={({ currentTarget }) => {
                    currentTarget.onerror = null; // prevents looping
                    currentTarget.src = UserImg;
                  }}
                ></img>
                <img
                  className={styles.container__content__information__left__edit}
                  src={Edit}
                  onClick={() => {
                    uploadFile();
                  }}
                ></img>
                <input
                  ref={fileUploadButton}
                  type="file"
                  name="filefield"
                  multiple
                  className="invisible"
                  onChange={handleFilesSelected}
                  accept="image/png, image/jpeg"
                ></input>
                <span
                  className={
                    styles['container__content__information__left-separator']
                  }
                ></span>
              </div>
              <div className={styles.container__content__information__right}>
                <SCForm
                  handleSubmit={handleSubmit}
                  register={register}
                  formState={formState}
                  onSubmit={editProfileAction}
                  control={control}
                >
                  <SCInput
                    label="Nombres"
                    name="name"
                    placeholder="Ingrese los nombres"
                    type="text"
                    className="col-12"
                  ></SCInput>
                  <SCInput
                    label="Apellidos"
                    name="lastName"
                    placeholder="Ingrese los apellidos"
                    type="text"
                    className="col-12"
                  ></SCInput>
                  <SCInput
                    label="Email"
                    name="email"
                    placeholder="Ingrese su email"
                    type="text"
                    className="col-12"
                  ></SCInput>
                  <SCInput
                    label="Contraseña"
                    name="password"
                    placeholder="Ingrese su contraseña"
                    type="password"
                    className="col-12"
                  ></SCInput>
                  <div
                    className={`${styles['container__content__information__right__submit']} full-width flex flex-center mt-20`}
                  >
                    <SCButton
                      loading={loading}
                      text="Editar"
                      disable={formState.isValid ? false : true}
                    ></SCButton>
                  </div>
                </SCForm>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default EditUserPage;
