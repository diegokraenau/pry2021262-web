import { Login } from '../../../../auth/commons/interfaces/login.interface';
import axios from 'axios';
import { User } from '@interfaces';

export const createUser = async (user: User) => {
  let userCreated: User | null = null;
  await axios
    .post(`${process.env.REACT_APP_PROXY_HOST}/v1/users`, user)
    .then((res) => {
      userCreated = res.data;
    });

  return userCreated;
};

export const getUserById = async (id: string) => {
  let userFound: User | null = null;
  await axios
    .get(`${process.env.REACT_APP_PROXY_HOST}/v1/users/${id}`)
    .then((res) => {
      userFound = res.data;
    });

  return userFound;
};

export const editUser = async (id: string, user: User) => {
  let userEdited: User | null = null;
  await axios
    .put(`${process.env.REACT_APP_PROXY_HOST}/v1/users/${id}`, user)
    .then((res) => {
      userEdited = res.data;
    });

  return userEdited;
};
