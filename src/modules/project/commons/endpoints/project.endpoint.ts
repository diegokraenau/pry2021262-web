import axios from 'axios';
import moment from 'moment';
import { Framework, GUI, Project } from '@interfaces';
import { View } from 'src/shared/interfaces/view';

export const getProjects = async (id: string) => {
  let projects: Project[] = [];
  await axios
    .get(`${process.env.REACT_APP_PROXY_HOST}/v1/users/${id}/projects`)
    .then((res) => {
      projects = res.data;
    });

  return projects;
};

export const createProject = async (userId: string, name: string) => {
  let project = null;
  await axios
    .post(`${process.env.REACT_APP_PROXY_HOST}/v1/projects`, {
      name,
      created_date: moment(new Date()).format('YYYY/MM/DD'),
      userId,
    })
    .then((res) => {
      project = res.data;
    });

  return project;
};

export const getProjectById = async (id: any): Promise<Project | null> => {
  let project = null;
  await axios
    .get(`${process.env.REACT_APP_PROXY_HOST}/v1/projects/${id}`)
    .then((res) => {
      project = res.data;
    });

  return project;
};

//TODO:Fix to class and add projectId
export const generateGui = async (
  wireframes: any,
  files: any,
  id: string
): Promise<GUI[] | null> => {
  const formData = new FormData();
  const arrViews = [];
  //Adding photos
  for (const file of files) {
    formData.append('files', file);
  }
  formData.append('projectId', id);
  //Format views array
  for (const wireframe of wireframes) {
    arrViews.push({
      imgName: wireframe.name,
      viewName: wireframe.nameView,
    });
  }
  formData.append('views', JSON.stringify(arrViews));
  let generated = null;
  await axios
    .post(
      `${process.env.REACT_APP_PROXY_HOST}/v1/wireframes/generate-GUI`,
      formData
    )
    .then((res) => {
      generated = res.data;
    });
  return generated;
};

export const getFrameworks = async () => {
  let frameworks: Framework[] = [];
  await axios
    .get(`${process.env.REACT_APP_PROXY_HOST}/v1/dependencies`)
    .then((res) => {
      frameworks = res.data;
    });

  return frameworks;
};

export const downloadProject = async (
  frameworkSelected: string,
  dependenciesSelected: any[],
  projectId: any
) => {
  await axios({
    url: `${process.env.REACT_APP_PROXY_HOST}/v1/projects/download`,
    method: 'POST',
    responseType: 'blob',
    data: {
      projectId: projectId,
      templateName: manageTemplateName(frameworkSelected),
      dependenciesIds:
        dependenciesSelected.length > 0
          ? dependenciesSelected?.map((x) => x.id)
          : [],
    },
  }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'project.zip'); //or any other extension
    document.body.appendChild(link);
    link.click();
  });
};

export const updateProject = async (id: string, content: Project) => {
  let project = null;
  await axios
    .put(`${process.env.REACT_APP_PROXY_HOST}/v1/projects/${id}`, content)
    .then((res) => {
      project = res.data;
    });
  return project;
};

export const updateView = async (id: string, content: View) => {
  let view = null;
  await axios
    .put(`${process.env.REACT_APP_PROXY_HOST}/v1/views/${id}`, content)
    .then((res) => {
      view = res.data;
    });
  return view;
};

export const updateProfilePicture = async (id: string, body: any) => {
  let imgUrl = null;
  await axios
    .put(
      `${process.env.REACT_APP_PROXY_HOST}/v1/users/${id}/update-photo`,
      body
    )
    .then((res) => {
      imgUrl = res.data;
    });
  return imgUrl;
};

export const exportViewOnlyHtml = async (
  projectId: string,
  viewsIds: string[]
) => {
  await axios({
    url: `${process.env.REACT_APP_PROXY_HOST}/v1/projects/download-only-html`,
    method: 'POST',
    responseType: 'blob',
    data: {
      projectId,
      viewsIds,
    },
  }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'project.zip'); //or any other extension
    document.body.appendChild(link);
    link.click();
  });
};

export const deleteProject = async (id: string) => {
  let project = null;
  await axios
    .delete(`${process.env.REACT_APP_PROXY_HOST}/v1/projects/${id}`)
    .then((res) => {
      project = res.data;
    });
  return project;
};

export const deleteView = async (id: string) => {
  let view = null;
  await axios
    .delete(`${process.env.REACT_APP_PROXY_HOST}/v1/views/${id}`)
    .then((res) => {
      view = res.data;
    });
  return view;
};

const manageTemplateName = (template: string) => {
  switch (template) {
    case 'ANGULAR':
      return 'Angular';
    case 'REACT':
      return 'React';
    case 'VUE':
      return 'Vue';
    default:
      break;
  }
};
