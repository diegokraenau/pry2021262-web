import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import {
  SCButton,
  SCColorPicker,
  SCComponentsDetected,
  SCPhotoSelector,
  SCPreview,
  SCStepper,
} from '@components';
import style from './AddWireframe.module.scss';
import BackImg from '@img/back.svg';
import { useContext } from 'react';
import { ProjectContext, MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import {
  generateGui,
  updateProject,
} from '@modules/project/commons/endpoints/project.endpoint';
import { GUI, Project } from '@interfaces';
import { useColor } from 'react-color-palette';

/*Global */
const steps = [
  { name: 'Wireframes' },
  { name: 'Componentes detectados' },
  { name: 'Previsualización' },
  { name: 'Estilos' },
];

const AddWireframe = () => {
  /*Variables */
  const { id } = useParams<any>();
  const [currentStep, setCurrentStep] = useState<number>(1);
  const [wireframes, setWireframes] = useState<any[]>([]);
  const history = useHistory();
  const { setMessageBus } = useContext(MessageBusContext);
  const [stepOneFormValid, setStepOneFormValid] = useState<boolean>(false);
  const [files, setFiles] = useState<any>(null);
  const [guis, setGUIs] = useState<GUI[]>([]);
  const [color, setColor] = useColor('hex', '#121212');
  const { projects, setProjects, setCopyProjects, copyProjects } =
    useContext(ProjectContext);

  /*Functions */
  const manageSteps = () => {
    switch (currentStep) {
      case 1:
        return (
          <SCPhotoSelector
            wireframes={wireframes}
            setWireframes={setWireframes}
            setStepOneFormValid={setStepOneFormValid}
            files={files}
            setFiles={setFiles}
          ></SCPhotoSelector>
        );
      case 2:
        return <SCComponentsDetected guis={guis}></SCComponentsDetected>;

      case 3:
        return <SCPreview guis={guis}></SCPreview>;

      case 4:
        return (
          <SCColorPicker color={color} setColor={setColor}></SCColorPicker>
        );

      default:
        return <></>;
    }
  };

  const customResponse = (item: GUI) => {
    item.uniqueTags = [...new Set(item.predictions!.map((x) => x.tagName))];
    item.template_decoded = window.atob(item.template);
    return item;
  };

  const generateGUI = async () => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Loader,
      data: {
        active: true,
        title: 'Procesando wireframes. . .',
      },
    });
    const generated = await generateGui(wireframes, files, id);
    if (generated) {
      //Forma unique tags
      for (let i = 0; i < generated.length; i++) {
        generated[i] = customResponse(generated[i]);
      }
      setMessageBus(null);
      setGUIs(generated);
      setCurrentStep(2);
    }
  };

  const updateProjectTheme = async () => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Loader,
      data: {
        active: true,
        title: 'Cambiando tema. . .',
      },
    });
    const content: Project = {
      theme: color.hex,
    };
    const projectUpdated = await updateProject(id, content).finally(() => {
      setMessageBus(null);
    });
    projectUpdated &&
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Success,
        data: {
          title: 'Generado exitosamente!',
          description: 'Sus wireframes han sido generados correctamente.',
          click: () => history.goBack(),
          close: () => history.goBack(),
        },
      });
  };

  const verifyName = (index: number, newName: string) => {
    const project = projects.filter((project: any) => project.id === id)[0];

    const filteredViews = project?.views?.filter((view: any) => {
      return view.name === newName;
    });

    const filteredNewViews = wireframes?.filter((view: any, j) => {
      if (j === index) return;
      return view.nameView === newName;
    });

    return (
      (filteredViews && filteredViews?.length !== 0) ||
      (filteredNewViews && filteredNewViews?.length !== 0)
    );
  };

  const managePropsButtons = () => {
    switch (currentStep) {
      case 1:
        return {
          text: 'Siguiente',
          disable: stepOneFormValid ? false : true,
          onClick: () => {
            const duplicates =
              wireframes.filter((newView: any, i) =>
                verifyName(i, newView.nameView)
              ) || [];
            console.log('duplica', duplicates);
            if (duplicates.length !== 0) {
              setMessageBus({
                channel: ChannelType.Modals,
                topic: TopicType.Error,
                data: {
                  title: 'Existen uno o más wireframes con el mismo nombre.',
                  description: 'Por favor, ingrese otro nombre.',
                  click: () => setMessageBus(null),
                  close: () => setMessageBus(null),
                },
              });
              return;
            }
            generateGUI();
          },
        };

      case 2:
        return {
          text: 'Siguiente',
          onClick: () => {
            setCurrentStep(3);
          },
        };

      case 3:
        return {
          text: 'Siguiente',
          onClick: () => {
            setCurrentStep(4);
          },
        };

      case 4:
        return {
          text: 'Finalizar',
          onClick: async () => {
            await updateProjectTheme();
          },
        };

      default:
        return {};
    }
  };

  /*State management */
  useEffect(() => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.AlertWarning,
      data: {
        title: 'Las reglas para nombrear wireframes son:',
        descriptions: [
          'No caracteres especiales,mayúsculas y números',
          'Utilizar este formato word-word',
          'No nombres duplicados',
          'No podrá cancelar la generación de los wireframes.',
        ],
      },
    });
  }, []);

  return (
    <div className={`${style.container} limit-container`}>
      <div className={style.container__content}>
        <div className={style.container__content__back}>
          <img
            src={BackImg}
            className={style['container__content__back-icon']}
            onClick={() => {
              history.goBack();
            }}
          ></img>
        </div>
        <SCStepper currentStep={currentStep} steps={steps}></SCStepper>
        {manageSteps()}
        <div className={`${style.container__content__options} mt-10`}>
          <SCButton {...managePropsButtons()}></SCButton>
        </div>
      </div>
    </div>
  );
};

export default AddWireframe;
