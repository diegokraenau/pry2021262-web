import ProjectHeader from '@modules/project/components/ProjectHeader/ProjectHeader';
import ProjectsList from '@modules/project/components/ProjectsList/ProjectsList';
import React, { useEffect } from 'react';
import style from './HomeProjectPage.module.scss';
import { useContext } from 'react';
import { AuthContext, ProjectContext, MessageBusContext } from '@contexts';
import { getProjects } from '../../commons/endpoints/project.endpoint';
import { ChannelType, TopicType } from '@enums';

const HomeProjectPage = () => {
  /*Variables */
  const { user } = useContext(AuthContext);
  const { setProjects , setCopyProjects} = useContext(ProjectContext);
  const { setMessageBus } = useContext(MessageBusContext);

  /*State management */
  useEffect(() => {
    if (user) {
      setMessageBus({
        channel: ChannelType.Global,
        topic: TopicType.Loader,
        data:{
          active: true,
          title:'Cargando proyectos. . .'
        }
      })
      getProjects(user.id).then((res) => {
        setProjects(res);
        setCopyProjects(res);
        setMessageBus(null);
      });
    }
  }, []);

  return (
    <div className={style.container}>
      <div className={`${style.container__content} limit-container`}>
        <ProjectHeader></ProjectHeader>
        <ProjectsList></ProjectsList>
      </div>
    </div>
  );
};

export default HomeProjectPage;
