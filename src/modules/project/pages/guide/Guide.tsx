import React from 'react';
import styles from './Guide.module.scss';
import GuideCard from './components/GuideCard';
import ElementsPdf from '@img/Elementos.pdf';

const Guide = () => {
  return (
    <div className={styles.container}>
      <div className={styles.guide__title}>GUIA DE USO</div>
      <p className="text-center mb-10">
        Wireframes a reconocer{' '}
        <a href={ElementsPdf} target="_blank">
          aquí
        </a>
        .
      </p>
      <div className={styles.guide}>
        <GuideCard step={1}>
          Para comenzar el diseño de un nuevo wireframe debemos tener un
          Proyecto para esto se debe clickear en el boton "Nuevo Proyecto" y
          otorgarle un nombre a este mismo.
        </GuideCard>
        <GuideCard step={2}>
          Seleccionaremos el proyecto que acabamos de crear y clickearemos en el
          boton de "Nuevo Wireframe" para poder empezar con el proceso de
          detección automatica de nuestros dibujos.
        </GuideCard>
        <GuideCard step={3}>
          En la pantalla de WireFrames procederemos a subir las imagenes de los
          dibujos para que el sistema analice, interprete y detecte los
          wireframes en las imagenes.
        </GuideCard>
        <GuideCard step={4}>
          Despues el sistema mostrará cada uno de los elementos detectados y se
          podrá pre-visualizar el modelo del wireframe. Posteriormente se podrá
          elegir un tema o paleta de colores para los elementos.
        </GuideCard>
        <GuideCard step={5}>
          Por ultimo, una vez que se hayan seleccionado las configuraciones de
          diseño, se podrán agregar dependencias y generar los wireframes en
          código para el framework deseado.
        </GuideCard>
      </div>
    </div>
  );
};

export default Guide;
