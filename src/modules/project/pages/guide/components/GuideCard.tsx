import React from 'react';
import styles from '../Guide.module.scss';

const GuideCard = ({ step, children }: any) => {
  return (
    <div
      className={`${styles.guide__card} ${
        step % 2 !== 0 ? styles.fadeLeft : styles.fadeRight
      }`}
    >
      <div className={styles.guide__step}>{step}.</div>
      <div className={styles.guide__description}>{children}</div>
    </div>
  );
};

export default GuideCard;
