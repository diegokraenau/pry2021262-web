import {
  downloadProject,
  exportViewOnlyHtml,
  getProjectById,
  updateProject,
} from '@modules/project/commons/endpoints/project.endpoint';
import React, { useEffect, useRef, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import style from './ProjectDetail.module.scss';
import { Project } from '@interfaces';
import { SCButton, SCSearchBar, SCTabs } from '@components';
import { useContext } from 'react';
import { ProjectContext, MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import ExportProject from '@modules/project/components/ExportProject/ExportProject';
import GUIList from '../../components/GUIList/GUIList';
import EmptySvg from '@img/no_data.svg';
import BackSvg from '@img/back.svg';
import Edit from '@img/edit.svg';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { validateWireframeName } from '@commons';

const ProjectDetail = () => {
  /*Variables */
  const { id } = useParams<any>();
  const history = useHistory();
  const { setMessageBus } = useContext(MessageBusContext);
  const [projectSelected, setProjectSelected] = useState<Project | null>(null);
  const [currentTab, setCurrentTab] = useState<number>(1);
  const [exportLoading, setExportLoading] = useState<boolean>(false);
  const [downloadLoading, setDownloadLoading] = useState<boolean>(false);
  const [isEditing, setEditing] = useState<boolean>(false);
  const titleInput = useRef<HTMLInputElement | null>(null);
  const { projects, setProjects, setCopyProjects, copyProjects } =
    useContext(ProjectContext);

  const tabElements = [
    {
      id: 1,
      name: 'Wireframes',
      click: () => {
        setCurrentTab(1);
        if (projectSelected && projectSelected.views) {
          setCopyViews(projectSelected.views);
        }
      },
    },
    {
      id: 2,
      name: 'Exportar en framework',
      click: () => {
        setFrameworkSelected('');
        setDependenciesSelected([]);
        setCurrentTab(2);
      },
    },
  ];

  const schema = yup.object().shape({
    name: yup
      .string()
      .test('test-name-view', 'Formato incorrecto', (value: any) => {
        const validation = validateWireframeName(value);
        return validation;
      })
      .required('Es necesario un nombre')
      .min(5, 'Mínimo 5 carácteres')
      .max(20, 'Máximo 20 caracteres'),
  });

  const { handleSubmit, register, formState, getValues, setValue, control } =
    useForm({
      mode: 'onChange',
      resolver: yupResolver(schema),
    });

  /*First stage */
  const [viewsSelected, setViewsSelected] = useState<string[]>([]);
  const [copyViews, setCopyViews] = useState<any[]>([]);

  /*Second stage */
  const [frameworkSelected, setFrameworkSelected] = useState<string>('');
  const [dependenciesSelected, setDependenciesSelected] = useState<any[]>([]);

  /*Functions */
  const goToNewWireframeAction = () => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Routing,
      data: {
        url: `/add-wireframe/${id}`,
      },
    });
  };

  const downloadProjectAction = async () => {
    setDownloadLoading(true);
    await downloadProject(frameworkSelected, dependenciesSelected, id).finally(
      () => setDownloadLoading(false)
    );
  };

  const manageTabs = (id: number) => {
    switch (id) {
      case 1:
        return (
          <>
            {copyViews && copyViews.length > 0 ? (
              <GUIList
                originalElements={projectSelected}
                views={copyViews}
                setViews={setCopyViews}
                setViewsSelected={setViewsSelected}
                setProjectSelected={setProjectSelected}
                // setCopyViews={setViews}
              ></GUIList>
            ) : (
              <div className={style['container__content__inside-empty']}>
                <img src={EmptySvg} alt="No data"></img>
                <p className="mt-40">No se encontraron elementos.</p>
              </div>
            )}
          </>
        );

      case 2:
        return (
          <ExportProject
            frameworkSelected={frameworkSelected}
            setFrameworkSelected={setFrameworkSelected}
            dependenciesSelected={dependenciesSelected}
            setDependenciesSelected={setDependenciesSelected}
          ></ExportProject>
        );

      default:
        return <></>;
    }
  };

  const manageButton = (id: number) => {
    switch (id) {
      case 1:
        return {
          text: 'Nuevo wireframe',
          onClick: () => {
            goToNewWireframeAction();
          },
        };

      case 2:
        return {
          loading: downloadLoading,
          text: 'Descargar',
          disable: manageValidDownload(),
          onClick: async () => {
            await downloadProjectAction();
          },
        };

      default:
        break;
    }
  };

  const manageValidDownload = () => {
    if (
      frameworkSelected &&
      projectSelected?.views &&
      projectSelected.views.length > 0
    )
      return false;
    return true;
  };

  const exportViewsOnlyHtml = async (projectId: string, viewsIds: string[]) => {
    setExportLoading(true);
    await exportViewOnlyHtml(projectId, viewsIds).finally(() =>
      setExportLoading(false)
    );
  };

  const openChangeThemeModal = () => {
    setMessageBus({
      channel: ChannelType.Modals,
      topic: TopicType.ChanegThemeModal,
      data: {
        colorCode: projectSelected?.theme,
        projectId: projectSelected?.id,
        projectSelected: projectSelected,
        setProjectSelected: setProjectSelected,
      },
    });
  };

  /*State management */
  useEffect(() => {
    if (id) {
      getProjectById(id).then((res) => {
        setProjectSelected(res);
        //Saving views
        if (res && res.views) {
          setCopyViews(res.views);
        }
      });
      console.log(projects);
    }
  }, []);

  const handleEditing = async () => {
    setEditing(false);
    if (projectSelected?.name === getValues(`name`)) {
      return;
    }

    let titleEdited =
      getValues(`name`) !== '' ? getValues(`name`) : projectSelected?.name;

    if (formState.errors.name) {
      titleEdited = projectSelected?.name;
      setValue('name', titleEdited, { shouldValidate: true });
    } else {
      if (projectSelected) {
        const validation = projects.filter(
          (project: { name: string }) =>
            project.name.toLowerCase() === getValues(`name`).toLowerCase()
        );
        if (validation.length !== 0) {
          setMessageBus({
            channel: ChannelType.Modals,
            topic: TopicType.Error,
            data: {
              title: 'Ya existe un proyecto con el mismo nombre.',
              description: 'Por favor, ingresé otro nombre.',
              click: () => setMessageBus(null),
              close: () => setMessageBus(null),
            },
          });
          return;
        }
        //Show spinner
        setMessageBus({
          channel: ChannelType.Global,
          topic: TopicType.Loader,
          data: {
            active: true,
            title: 'Actualizando. . .',
          },
        });
        //Manage update
        await updateProject(projectSelected.id!, {
          name: titleEdited,
        })
          .then(() => {
            setProjectSelected({
              ...projectSelected,
              name: titleEdited,
            });
          })
          .finally(() => setMessageBus(null));
      }
    }
  };

  return (
    <div className={`${style.container} limit-container`}>
      {projectSelected && (
        <div className={style.container__content}>
          <div className={`${style['container__content__title']} mb-20`}>
            <img src={BackSvg} onClick={() => history.goBack()}></img>
            <div>
              {isEditing ? (
                <>
                  <input
                    className={style['container__content__title-edit']}
                    {...register(`name` as any)}
                    placeholder="Ingrese el nombre"
                    defaultValue={projectSelected.name}
                  ></input>
                  {formState.errors.name && (
                    <p
                      className={
                        style['container__content__options-name-error']
                      }
                    >
                      {formState.errors.name.message}
                    </p>
                  )}
                </>
              ) : (
                projectSelected.name &&
                projectSelected.name[0].toUpperCase() +
                  projectSelected.name.substring(1, projectSelected.name.length)
              )}
              <img
                src={Edit}
                onClick={() => {
                  isEditing ? handleEditing() : setEditing(true);
                }}
              ></img>
            </div>
          </div>
          <div className={style.container__content__searchbar}>
            {currentTab == 1 && (
              <div className={style['container__content__searchbar-limit']}>
                <SCSearchBar
                  originalElements={projectSelected.views}
                  setCopyElements={setCopyViews}
                  placeHolder="Buscar wireframe"
                ></SCSearchBar>
              </div>
            )}
            {currentTab == 2 && (
              <div
                className={`${style.container__content__information} flex flex-end`}
              >
                <p>
                  En esta sección podrás exportar tus wireframes con un
                  framework y dependencias seleccionas.
                </p>
              </div>
            )}
          </div>
          <div className={style['container__content__sl']}>
            <div className={style['container__content__sl__tabs']}>
              <SCTabs currentTab={currentTab} elements={tabElements}></SCTabs>
            </div>
            {currentTab == 1 ? (
              <div className={style['container__content__sl__buttons']}>
                <SCButton {...manageButton(currentTab)}></SCButton>
                {currentTab == 1 && (
                  <SCButton
                    text="Exportar"
                    outline={true}
                    onClick={() => exportViewsOnlyHtml(id, viewsSelected)}
                    loading={exportLoading}
                    disable={
                      projectSelected?.views && projectSelected.views.length > 0
                        ? false
                        : true
                    }
                  ></SCButton>
                )}
              </div>
            ) : (
              <SCButton {...manageButton(currentTab)}></SCButton>
            )}
          </div>
          <div className={style.container__content__inside}>
            {manageTabs(currentTab)}
          </div>
          <div className="w-r limit-container">
            <div
              className={style.container__content__corner}
              onClick={() => openChangeThemeModal()}
            >
              <div
                className={style.container__content__corner__theme}
                style={{
                  background: projectSelected?.theme
                    ? projectSelected?.theme
                    : '#0049BD',
                }}
              ></div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProjectDetail;
