import React, { useEffect, useState } from 'react';
import { SCSearchBar, SCSelectMultiple, SCSingleSelect } from '@components';
import { Framework } from '@interfaces';
import style from './ExportProject.module.scss';
import { getFrameworks } from '../../commons/endpoints/project.endpoint';
import { useContext } from 'react';
import { MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import DependenciesList from '../DependenciesList/DependenciesList';
import { showGlobalWarningAlert } from '@alerts/global-warning.alert';

interface IProps {
  frameworkSelected: any;
  setFrameworkSelected: any;
  dependenciesSelected: any;
  setDependenciesSelected: any;
}

const ExportProject = ({
  frameworkSelected,
  setFrameworkSelected,
  dependenciesSelected,
  setDependenciesSelected,
}: IProps) => {
  /*Variables */
  const [frameworks, setFrameworks] = useState<Framework[]>([]);
  const [dependencies, setDependencies] = useState<any[]>([]);
  const [copyDependenciesArray, setCopyDependenciesArray] = useState<string[]>(
    []
  );
  const { setMessageBus } = useContext(MessageBusContext);
  const frameworkOptions = [
    {
      name: 'Angular',
      click: () => {
        setFrameworkSelected('ANGULAR');
        setDependenciesSelected([]);
      },
    },
    {
      name: 'Vue',
      click: () => {
        setFrameworkSelected('VUE');
        setDependenciesSelected([]);
      },
    },
    {
      name: 'React',
      click: () => {
        setFrameworkSelected('REACT');
        setDependenciesSelected([]);
      },
    },
  ];

  /*Functions */

  /*State management */
  useEffect(() => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Loader,
      data: {
        active: true,
        title: 'Cargando. . .',
      },
    });
    getFrameworks().then((res) => {
      setFrameworks(res);
      setMessageBus(null);
    });
    setFrameworkSelected('ANGULAR');
    showGlobalWarningAlert(
      'Mínimo 1 wireframe adjuntado para exportar con un framework.'
    );
  }, []);

  useEffect(() => {
    if (frameworkSelected && frameworks?.length > 0) {
      setDependencies(
        frameworks.find((x: any) => x.framework == frameworkSelected)!
          .dependencies!
      );
      setCopyDependenciesArray(
        frameworks.find((x: any) => x.framework == frameworkSelected)!
          .dependencies!
      );
    }
  }, [frameworkSelected, frameworks]);

  return (
    <div className={style.container}>
      {frameworks.length > 0 && (
        <>
          <div className={style.container__configuration}>
            <p className="mb-15">Framework:</p>
            <SCSingleSelect options={frameworkOptions}></SCSingleSelect>
            <p className="mt-15">Dependencias: </p>
            <SCSearchBar
              originalElements={dependencies}
              setCopyElements={setCopyDependenciesArray}
              placeHolder="Buscar dependencia"
            ></SCSearchBar>
            <SCSelectMultiple
              options={copyDependenciesArray}
              direction="vertically"
              updateSelectedArray={setDependenciesSelected}
            ></SCSelectMultiple>
          </div>
          <div className={style.container__dependencies}>
            <DependenciesList
              elements={dependenciesSelected}
              updateSelectedArray={setDependenciesSelected}
            ></DependenciesList>
          </div>
        </>
      )}
    </div>
  );
};

export default ExportProject;
