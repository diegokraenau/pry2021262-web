import React, { useState, useContext, useEffect } from 'react';
import style from './ProjectsList.module.scss';
import { ProjectContext } from '@contexts';
import ProjectCard from '../ProjectCard/ProjectCard';
import { Project } from '@interfaces';
import ArrowNext from '@img/arrow_next.svg';
import ArrowBack from '@img/arrow_back.svg';
import NoDataImg from '@img/no_data_v2.svg';

const ProjectsList = () => {
  /* Variables*/
  const { copyProjects, setCopyProjects, projects } =
    useContext(ProjectContext);
  const [itemsPerPage, setItemsPerPage] = useState<number>(0);
  const [currentPage, setCurrentPage] = useState<number>(1);

  const arrowNextAction = () => {
    if (itemsPerPage + 8 < copyProjects.length) {
      setItemsPerPage((value) => value + 8);
      setCurrentPage(currentPage + 1);
    }
  };

  const arrowBackAction = () => {
    if (itemsPerPage - 8 >= 0) {
      setItemsPerPage((value) => value - 8);
      setCurrentPage(currentPage - 1);
    }
  };

  useEffect(() => {
    if (projects && projects.length > 0) {
      setCopyProjects(projects);
      setItemsPerPage(0);
    }
  }, [projects]);

  useEffect(() => {
    if (copyProjects && copyProjects.length > 0) {
      setItemsPerPage(0);
      setCurrentPage(1);
    }
  }, [copyProjects]);

  return (
    <div className={style.container}>
      <div className={style.container__content}>
        <h1 className="mb-10">Proyectos</h1>
        {copyProjects ? (
          <>
            {copyProjects.length > 0 && (
              <>
                <img
                  className={style.container__content_next}
                  src={ArrowNext}
                  onClick={() => arrowNextAction()}
                ></img>
                <img
                  className={style.container__content_back}
                  src={ArrowBack}
                  onClick={() => arrowBackAction()}
                ></img>
              </>
            )}
            {copyProjects.length > 0 ? (
              <div className={style.container__content__cards}>
                {copyProjects
                  .slice(itemsPerPage, itemsPerPage + 8)
                  .map((x: Project) => (
                    <ProjectCard project={x} key={x.id}></ProjectCard>
                  ))}
              </div>
            ) : (
              <div className={style.container__content__empty}>
                <img src={NoDataImg} alt="No data"></img>
                <p className="mt-40">No se encontraron proyectos.</p>
              </div>
            )}
            {copyProjects.length > 0 && (
              <span className={style.container__content__footer}>
                Página {currentPage} de {Math.ceil(copyProjects?.length / 8)}
              </span>
            )}
          </>
        ) : (
          <p>No tiene proyectos por el momento.</p>
        )}
      </div>
    </div>
  );
};

export default ProjectsList;
