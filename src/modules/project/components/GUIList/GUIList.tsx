import React, {
  useRef,
  useEffect,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import style from './GUIList.module.scss';
import Delete from '@img/delete.svg';
import Edit from '@img/edit.svg';
import html2canvas from 'html2canvas';
import { useContext } from 'react';
import { ProjectContext, MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { validateWireframeName } from '@commons';
import {
  deleteView,
  updateView,
} from '@modules/project/commons/endpoints/project.endpoint';

interface IProps {
  views: any;
  setViews: any;
  setViewsSelected: any;
  originalElements: any;
  setProjectSelected: any;
  // setCopyViews: any;
}

const GUIList = ({
  views,
  setViewsSelected,
  originalElements,
  setViews,
  setProjectSelected,
}: IProps) => {
  /*Functions */
  const nodeArr = useRef<any[]>([]);
  const [visibleContent, setVisibleContent] = useState(false);
  const { setMessageBus } = useContext(MessageBusContext);
  const [isEditing, setEditing] = useState<boolean[]>([]);
  // const titleInput = useRef<HTMLInputElement | null>(null);

  const formSchema = {
    nameView: yup
      .string()
      .test('test-name-view', 'Formato incorrecto', (value: any) => {
        const validation = validateWireframeName(value);
        return validation;
      })
      .min(5, 'Mínimo 5 letras')
      .required('Nombre requerido'),
  };

  const fieldsSchema = yup.object().shape({
    views: yup.array().of(yup.object().shape(formSchema)),
  });

  const { control, register, formState, handleSubmit, getValues, setValue } =
    useForm({
      mode: 'onChange',
      resolver: yupResolver(fieldsSchema),
    });

  const manageHtml = (x: any) => {
    return window.atob(x.template);
  };

  const deleteViewAction = async (id: any) => {
    const deleted = await deleteView(id);
    if (deleted) {
      setViews((data: any) => data.filter((x: any) => x.id !== id));
      const projectSelectedCopy = { ...originalElements };
      projectSelectedCopy.views = originalElements.views.filter(
        (x: any) => x.id !== id
      );
      setProjectSelected(projectSelectedCopy);
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Success,
        data: {
          title: 'Eliminación exitosa',
          description: 'Se eliminó la vista correctamente.',
          click: () => setMessageBus(null),
          close: () => setMessageBus(null),
        },
      });
    }
  };

  const openDeleteViewModal = (id: any) => {
    setMessageBus({
      channel: ChannelType.Modals,
      topic: TopicType.DecisionModal,
      data: {
        title: '¿Desea eliminar la vista?',
        description: 'Se eliminará la vista y no podrá recuperarlo.',
        click: () => deleteViewAction(id),
      },
    });
  };
  const onCapture = async () => {
    const newArr: any[] = [];
    let i = 0;
    for (const view of originalElements.views) {
      await html2canvas(nodeArr.current[i].contentDocument.body, {
        useCORS: true,
        allowTaint: true,
        logging: true,
        scale: 0.5,
      }).then(function (canvas: any) {
        const img = canvas.toDataURL('image/png');
        // const p = document.getElementById(`img-gui-${i}`) as any;
        if (view.test) {
          setMessageBus(null);
          setVisibleContent(true);
          return;
        }
        view.test = img;
        newArr.push(view);
        // setImage(img);
        // p.src = img;
        i++;
        if (i == views.length) {
          setViews(newArr);
          setMessageBus(null);
          setVisibleContent(true);
        }
        // html2canvas(document.querySelector('#capture'),{allowTaint: true, useCORS: true}).then((canvas) => {
        //   document.body.appendChild(canvas);
        // });
      });
    }
  };

  const manageClick = (x: any, e: any) => {
    setViewsSelected((arr: any[]) => {
      if (arr.find((y: any) => y == x.id)) {
        return arr.filter((z) => z != x.id);
      } else {
        return [...arr, x.id];
      }
    });
  };

  /*State management */
  useEffect(() => {
    if (views && views.length > 0) {
      views.forEach((x: boolean) => setEditing((data) => [...data, false]));
      setMessageBus({
        channel: ChannelType.Global,
        topic: TopicType.Loader,
        data: {
          active: true,
          text: 'Cargando. . .',
        },
      });
      setTimeout(() => {
        onCapture();
      }, 1000);
      // setMessageBus(null);
    }
  }, []);

  const handleEditing = async (
    index: any,
    value: boolean,
    id?: string,
    oldValue?: string
  ) => {
    if (!value) {
      if (formState.errors.views?.[index]?.nameView) {
        setValue(`views[${index}].nameView`, oldValue, {
          shouldValidate: true,
        });
        setViews((data: any) =>
          data.map((x: any, i: any) => {
            if (i === index) {
              x.name = oldValue;
              return x;
            }
            return x;
          })
        );
      } else {
        const currentValue = getValues(`views[${index}].nameView`);
        const isDuplicate = views.filter(
          (view: any) => view.name === currentValue
        );
        if (isDuplicate.length !== 0) {
          id = undefined;
          setMessageBus({
            channel: ChannelType.Modals,
            topic: TopicType.Error,
            data: {
              title: 'Ya existe una vista con el mismo nombre.',
              description: 'Por favor, ingresé otro nombre.',
              click: () => setMessageBus(null),
              close: () => setMessageBus(null),
            },
          });
          setValue(`views[${index}].nameView`, oldValue, {
            shouldValidate: true,
          });
        }

        //Manage update
        if (id) {
          //Show spinner
          setMessageBus({
            channel: ChannelType.Global,
            topic: TopicType.Loader,
            data: {
              active: true,
              title: 'Actualizando. . .',
            },
          });

          if (currentValue === oldValue) {
            const newEditing = [...isEditing];
            newEditing[index] = value;
            setEditing(newEditing);
            setMessageBus(null);
            return;
          }

          await updateView(id, {
            name: currentValue,
          })
            .then(() => {
              setValue(`views[${index}].nameView`, currentValue, {
                shouldValidate: true,
              });
              setViews((data: any) =>
                data.map((x: any, i: any) => {
                  if (i === index) {
                    x.name = currentValue;
                    return x;
                  }
                  return x;
                })
              );
            })
            .finally(() => setMessageBus(null));
        }
      }
    }

    setEditing((data) =>
      data.map((x, i) => {
        if (i === index) {
          return value;
        }
        return x;
      })
    );
  };

  return (
    <div className={style.container}>
      <div
        className={`${style.container__content} ${!visibleContent && 'hidden'}`}
      >
        {views.map((x: any, index: any) => (
          <div className={style.container__content__item} key={x.id}>
            <div className={style.container__content__item__select}>
              <input
                type="checkbox"
                value={x.id}
                onClick={(e: any) => manageClick(x, e)}
              ></input>
            </div>
            <div className={style.container__content__item__preview}>
              <div className={style.container__content__item__img}>
                <img id={`img-gui-${index}`} src={x.test || ''}></img>
              </div>
              <iframe
                id={`iframe-gui-${index}`}
                className={`${style['container__content__item__preview-photo']}`}
                srcDoc={manageHtml(x)}
                ref={(el) => (nodeArr.current[index] = el)}
              ></iframe>
            </div>
            <div className={style.container__content__item__options}>
              <p className={style['container__content__item__options-name']}>
                {isEditing[index] === true ? (
                  <>
                    <input
                      className={
                        style['container__content__item__options-name-input']
                      }
                      {...register(`views.${index}.nameView` as any)}
                      placeholder="Ingrese el nombre"
                      defaultValue={x.name}
                    ></input>
                    {formState.errors.views?.[index]?.nameView && (
                      <p
                        className={
                          style['container__content__item__options-name-error']
                        }
                      >
                        {formState.errors.views?.[index]?.nameView.message}
                      </p>
                    )}
                  </>
                ) : (
                  x.name
                )}
              </p>
              <div
                className={style['container__content__item__options-buttons']}
              >
                <img
                  src={Edit}
                  onClick={() => {
                    isEditing[index]
                      ? handleEditing(index, false, x.id, x.name)
                      : handleEditing(index, true);
                  }}
                ></img>
                <img
                  src={Delete}
                  onClick={() => openDeleteViewModal(x.id)}
                ></img>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default GUIList;
