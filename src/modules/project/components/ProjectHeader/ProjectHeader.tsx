import React from 'react';
import style from './ProjectHeader.module.scss';
import { useContext } from 'react';
import { AuthContext, MessageBusContext, ProjectContext } from '@contexts';
import { SCButton, SCSearchBar } from '@components';
import { ChannelType, TopicType } from '@enums';
import User from '@img/user.svg';

const ProjectHeader = () => {
  /*Variables */
  const { user } = useContext(AuthContext);
  const { setMessageBus } = useContext(MessageBusContext);
  const { projects, setCopyProjects } = useContext(ProjectContext);

  /*Functions */
  const newProject = () => {
    setMessageBus({
      channel: ChannelType.Modals,
      topic: TopicType.AddProject,
    });
  };

  return (
    <div className={style.container}>
      <div className={style.container__content}>
        <div
          className={`${style.container__content__fl} flex flex-space-between`}
        >
          <div className={style.container__content__fl__info}>
            <div className={`${style['container__content__fl__info-img']}`}>
              <img
                className="global-shadow"
                src={user?.imgUrl}
                onError={({ currentTarget }) => {
                  currentTarget.onerror = null; // prevents looping
                  currentTarget.src = User;
                }}
              ></img>
            </div>
            <div
              className={`${style['container__content__fl__info-text']} ml-30`}
            >
              <p className={style['container__content__fl__info-text-name']}>
                {user?.name + ', ' + user?.lastName}
              </p>
              <p className={style['container__content__fl__info-text-title']}>
                Mi biblioteca
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${style.container__content__sl} flex flex-space-between flex-y-center mt-10`}
        >
          <div className={style['container__content__sl-searchbar-content']}>
            <SCSearchBar
              originalElements={projects}
              setCopyElements={setCopyProjects}
              placeHolder="Buscar proyecto"
            ></SCSearchBar>
          </div>
          <SCButton
            text="Nuevo proyecto"
            onClick={() => {
              newProject();
            }}
          ></SCButton>
        </div>
      </div>
    </div>
  );
};

export default ProjectHeader;
