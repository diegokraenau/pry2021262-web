import React from 'react';
import style from './ProjectCard.module.scss';
import Delete from '@img/delete.svg';
import Edit from '@img/edit.svg';
import { useContext } from 'react';
import { MessageBusContext, ProjectContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import { deleteProject } from '@modules/project/commons/endpoints/project.endpoint';
import { Project } from '@interfaces';

const ProjectCard = (props: any) => {
  /*Variables */
  const { project } = props;
  const { setMessageBus } = useContext(MessageBusContext);
  const { setProjects, setCopyProjects } = useContext(ProjectContext);

  /*Functions */
  const goToDetail = (id: any) => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Routing,
      data: {
        url: `/projects/${id}`,
      },
    });
  };

  const deleteProjectAction = async () => {
    const deleted = await deleteProject(project?.id);
    if (deleted) {
      setProjects((data: Project[]) =>
        data.filter((x) => x.id !== project?.id)
      );
      setCopyProjects((data: Project[]) =>
        data.filter((x) => x.id !== project?.id)
      );
      // setMessageBus(null);
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Success,
        data: {
          title: 'Eliminación exitosa',
          description: 'Se eliminó el proyecto correctamente.',
          click: () => setMessageBus(null),
          close: () => setMessageBus(null),
        },
      });
    }
  };

  const openDeleteProjectModal = () => {
    setMessageBus({
      channel: ChannelType.Modals,
      topic: TopicType.DecisionModal,
      data: {
        title: '¿Desea eliminar el proyecto?',
        description: 'Se eliminará el proyecto y no podrá recuperarlo.',
        click: () => deleteProjectAction(),
      },
    });
  };

  return (
    <div className={style.container}>
      <div className={style.container__content}>
        <div className={`${style.container__content__fl} flex flex-center`}>
          <p className={style['container__content__fl-symbol']}>
            {project?.name.charAt(0).toUpperCase()}
          </p>
        </div>
        <div className={style.container__content__sl}>
          <p
            className={`${style['container__content__sl-project']} mt-10 mb-10`}
          >
            {project.name &&
              project.name[0].toUpperCase() +
                project.name.substring(1, project.name.length)}
          </p>
          <div
            className={`${style.container__content__sl__option} flex flex-space-between`}
          >
            <p className={`${style['container__content__sl__option-date']}`}>
              {project?.created_date}
            </p>
            <div
              className={`${style['container__content__sl__option-buttons']} flex flex-space-between`}
            >
              <img
                src={Edit}
                onClick={() => {
                  goToDetail(project?.id);
                }}
              ></img>
              <img src={Delete} onClick={() => openDeleteProjectModal()}></img>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProjectCard;
