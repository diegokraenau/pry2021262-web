import React from 'react';
import style from './DependenciesList.module.scss';

interface IProps {
  elements: any[];
  updateSelectedArray: any;
}

const DependenciesList = ({ elements, updateSelectedArray }: IProps) => {
  /*Functions */

  const manageClick = (x: any, e: any) => {
    updateSelectedArray((arr: any[]) => {
      if (arr.find((y: any) => y == x)) {
        return arr.filter((z) => z != x);
      } else {
        return [...arr, x];
      }
    });
  };

  return (
    <div className={style.container}>
      <p className="mb-10">Dependencias elegidas:</p>
      <div className={style.container__content}>
        {elements && elements.length > 0 ? (
          elements.map((x: any) => (
            <div className={style.container__content__item} key={x.name}>
              <input
                checked
                disabled
                type="checkbox"
                name="option"
                value={x.name}
              ></input>
              <p>{x.name}</p>
            </div>
          ))
        ) : (
          <p className="mt-10">No hay dependencias seleccionadas.</p>
        )}
      </div>
    </div>
  );
};

export default DependenciesList;
