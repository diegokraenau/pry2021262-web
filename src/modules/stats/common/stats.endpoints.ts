import axios from 'axios';
import { forkJoin } from 'rxjs';



export const getData = async () => {
    let response: any[] = []
    try {
        const totalData = axios.get(`${process.env.REACT_APP_PROXY_HOST}/v1/history/totalData`);
        const frameworkData = axios.get(`${process.env.REACT_APP_PROXY_HOST}/v1/history/frameworkData`);
        const timeData = axios.get(`${process.env.REACT_APP_PROXY_HOST}/v1/history/timeData`);

        await Promise.all([totalData, frameworkData, timeData]).then((values) => {
            response = values.map((x) => x.data)
        });
    }
    catch (error) {
        console.log(error)
    }
    return response

}
