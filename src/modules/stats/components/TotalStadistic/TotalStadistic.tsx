import React from 'react'
import style from './TotalStadistic.module.scss';

type StatsProps = {
    center: boolean,
    nombre: string,
    numero: number
}

const TotalStadistic = ({ center, nombre, numero }: StatsProps) => {
    return (
        <div className={`${center ? style.Center : style.container}`}>
            <div className={style.Number}>
                <h1>{numero}</h1>
            </div>
            <div className={style.Name}>
                <h2>{nombre}</h2>
            </div>
        </div>
    )
}

export default TotalStadistic