import React from 'react'
import style from './StadisticChart.module.scss';
import { Line } from 'react-chartjs-2'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const StadisticChart = (data: []) => {
    const arr = data.data;
    const dias = Object.keys(arr).map(d => {
        return new Date(arr[d].created_at).toLocaleDateString();
    });
    const cantidad = Object.keys(arr).map(d => {
        return arr[d].Cantidad;
    });

    const chartdata = {
        labels: dias,
        datasets: [
            {
                data: cantidad,
                backgroundColor: "rgba(75,192,192,0.2)",
                borderColor: "rgba(75,192,192,1)",
                pointStyle: 'circle',
                pointRadius: 10,
                pointHoverRadius: 15
            }
        ]
    };
    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Proyectos descargados por día',
                position: 'top' as const,
                align: 'start',
                font: { size: 24 }
            },
            layout: {
                padding: {
                    left: 40
                }
            }
        },
    };

    return (
        <div className={style.container}>
            <Line data={chartdata} options={options} />
        </div>
    )
}

export default StadisticChart