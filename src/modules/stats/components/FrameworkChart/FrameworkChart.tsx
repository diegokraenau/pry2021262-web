import React from 'react'
import style from './FrameworkChart.module.scss';
import { Pie } from 'react-chartjs-2'
import ChartDataLabels from 'chartjs-plugin-datalabels';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement,

} from 'chart.js';
import { roundTo } from '@modules/stats/common/helper';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement,
    ChartDataLabels
);

const FrameworkChart = (data: any[]) => {

    const totalR = roundTo(data?.data.totalReact, 2);
    const totalA = roundTo(data?.data.totalAngular, 2);
    const totalV = roundTo(data?.data.totalVue, 2);

    const chartData = {
        labels: ['React', 'Angular', 'Vue'],
        datasets: [
            {
                labels: ['React', 'Angular', 'Vue'],
                data: [totalR, totalA, totalV],
                backgroundColor: [
                    '#61DBFB',
                    '#DD1B16',
                    '#42B883'
                ],
                borderColor: [

                    '#60DAFB',
                    '#A6120D',
                    '#35495E'
                ],

                borderwidth: 1
            },
        ],
    };

    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                display: true,
                position: 'top' as const,
                align: 'end',
                labels: {
                    font: {
                        size: 30
                    }
                }
            },
            datalabels: {
                display: true,
                color: "white",
                font: { size: 30 },
                formatter: function (value: number, context: any) {
                    const display = [`${context.chart.data.labels[context.dataIndex]}`, `${value}%`]
                    return display;
                }
            },
            title: {
                display: true,
                text: 'Frameworks descargados por proyectos',
                position: 'top' as const,
                align: 'start',
                font: { size: 30 }
            },
            layout: {
                padding: {
                    left: 40
                }
            }
        },
    };

    return (
        <div className={style.container} >
            <Pie data={chartData} plugins={[ChartDataLabels]} options={options} />
        </div >
    )
}
export default FrameworkChart