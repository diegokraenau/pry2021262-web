import React, { useState, useEffect, useContext } from 'react'
import TotalStadistic from '../components/TotalStadistic/TotalStadistic';
import style from './StadisticPage.module.scss';
import { getData } from '../common/stats.endpoints'
import StadisticChart from '../components/StadisticChart/StadisticChart';
import FrameworkChart from '../components/FrameworkChart/FrameworkChart';
import { MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';



const Stadistics = () => {

    const [response, setResponse] = useState<any>([]);
    const { setMessageBus } = useContext(MessageBusContext)

    useEffect(() => {
        setMessageBus({
            channel: ChannelType.Global,
            topic: TopicType.Loader,
            data: {
                active: true,
                title: 'Cargando. . .',
            },
        });
        getData().then(res => {
            setResponse(res);
        }).finally(() => {
            setMessageBus(null)
        })
    }, [])
    return (
        <div className={`${style.container} `}>
            {
                response && response.length > 0 &&
                (<div className={style.container__content}>
                    <div className={style.Title}>
                        <h1>Resumen del sistema</h1>
                    </div>
                    <div className={style.container__gridStats}>
                        <div className={style.container__gridData}>
                            <TotalStadistic center={false} nombre={"Total Usuarios"} numero={response[0].totalUsers} />

                            <TotalStadistic center={true} nombre={"Total Gui Generadas"} numero={response[0].totalWireframes} />

                            <TotalStadistic center={false} nombre={"Total de Proyectos Descargados"} numero={response[0].totalDownloads} />
                        </div>
                    </div>
                    <div className={style.container__chart}>
                        <StadisticChart data={response[2]} />
                    </div>
                    <div className={style.container__framechart}>
                        <FrameworkChart data={response[1]} />
                    </div>
                </div>
                )
            }
        </div>
    )
}

export default Stadistics