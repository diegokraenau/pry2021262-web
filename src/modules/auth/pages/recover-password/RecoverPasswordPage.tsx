import React, { useState, useContext } from 'react';
import styles from './RecoverPasswordPage.module.scss';
import RecoverPasswordImg from '@img/recover_password.svg';
import BackImg from '@img/back.svg';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { SCForm, SCInput, SCButton } from '@components';
import { useHistory } from 'react-router-dom';
import { MessageBusContext } from '../../../../shared/contexts/MessageBusContext';
import { ChannelType, TopicType } from '@enums';
import { sendEmail } from '@modules/auth/commons/endpoints/auth.endpoint';

const RecoverPasswordPage = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const { setMessageBus } = useContext(MessageBusContext);
  const history = useHistory();
  const schema = yup.object().shape({
    email: yup
      .string()
      .email('Es necesario un correo')
      .required('Es necesario un correo')
      .min(10, 'Mínimo 10 caracteres')
      .max(100, 'Máximo 100 caracteres'),
  });

  const { handleSubmit, register, formState, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  /*Functions */
  const sendEmailAction = async (data: any) => {
    setLoading(true);
    const sent = await sendEmail(data?.email);
    if (sent) {
      setLoading(false);
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Success,
        data: {
          title: 'Envio exitoso',
          description: 'Se envio el correo de forma correcta.',
          click: () => {
            history.push('/');
            setMessageBus(null);
          },
          close: () => {
            history.push('/');
            setMessageBus(null);
          },
        },
      });
    }
  };

  return (
    <div className={styles.container}>
      <img
        className={styles['container-back']}
        src={BackImg}
        alt="back_arrow"
        onClick={() => history.push('/')}
      ></img>
      <div className={styles.container__content}>
        <h1 className="mb-25">Recuperar contraseña</h1>
        <img src={RecoverPasswordImg} alt="recover_password"></img>
        <SCForm
          handleSubmit={handleSubmit}
          register={register}
          formState={formState}
          onSubmit={sendEmailAction}
          control={control}
          className="flex flex-x-center"
        >
          <SCInput
            label="Email"
            name="email"
            placeholder="Ingrese el email"
            type="text"
            className="col-6"
          ></SCInput>
          <div className={`full-width flex flex-center mt-20`}>
            <SCButton
              loading={loading}
              text="Enviar email"
              disable={formState.isValid ? false : true}
            ></SCButton>
          </div>
        </SCForm>
      </div>
    </div>
  );
};

export default RecoverPasswordPage;
