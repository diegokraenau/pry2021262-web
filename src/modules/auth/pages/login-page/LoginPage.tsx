import React from 'react';
import style from './LoginPage.module.scss';
import BigImage from '@img/big_image.svg';
import LoginForm from '../../components/login-form/LoginForm';

const LoginPage = () => {
  return (
    <div className={style.container}>
      <div className={style.container__content}>
        <div className={`${style.container__content__left} flex flex-center`}>
          <div className={style['container__content__left-img']}>
            <img src={BigImage}></img>
          </div>
        </div>
        <div className={`${style.container__content__right} flex flex-center`}>
          <LoginForm />
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
