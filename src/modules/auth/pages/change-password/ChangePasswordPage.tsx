import React, { useContext, useState } from 'react';
import styles from './ChangePasswordPage.module.scss';
import ChangePasswordImg from '@img/change_password.svg';
import BackImg from '@img/back.svg';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { SCForm, SCInput, SCButton } from '@components';
import { useHistory, useParams } from 'react-router-dom';
import { editUser } from '@modules/user/pages/commons/endpoints/user.endpoint';
import { MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';

const ChangePasswordPage = () => {
  const { id } = useParams<any>();
  const { setMessageBus } = useContext(MessageBusContext);
  const [loading, setLoading] = useState<boolean>(false);
  const history = useHistory();
  const schema = yup.object().shape({
    password: yup
      .string()
      .required('Es necesario una contraseña')
      .min(5, 'Mínimo 5 caracteres')
      .max(20, 'Máximo 20 caracteres'),
    clonePassword: yup
      .string()
      .required('Es necesario una contraseña')
      .min(5, 'Mínimo 5 caracteres')
      .max(20, 'Máximo 20 caracteres')
      .oneOf([yup.ref('password'), null], 'Contraseñas deben coincidir'),
  });

  const { handleSubmit, register, formState, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  /*Functions */
  const changePasswordAction = async (data: any) => {
    setLoading(true);
    const edited = await editUser(id, { password: data?.password });
    if (edited) {
      setLoading(false);
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Success,
        data: {
          title: 'Cambio exiotoso',
          description: 'Se cambio las credenciales correctamente.',
          click: () => history.push('/'),
          close: () => history.push('/'),
        },
      });
    }
  };

  return (
    <div className={styles.container}>
      <img
        className={styles['container-back']}
        src={BackImg}
        alt="back_arrow"
        onClick={() => history.push('/')}
      ></img>
      <div className={styles.container__content}>
        <h1 className="mb-25">Cambiar contraseña</h1>
        <img src={ChangePasswordImg} alt="chane_password"></img>
        <SCForm
          handleSubmit={handleSubmit}
          register={register}
          formState={formState}
          onSubmit={changePasswordAction}
          control={control}
          className="flex flex-x-center"
        >
          <SCInput
            label="Contraseña"
            name="password"
            placeholder="Ingrese la contraseña"
            type="password"
            className="col-7"
          ></SCInput>
          <SCInput
            label="Confirmar contraseña"
            name="clonePassword"
            placeholder="Ingrese la contraseña"
            type="password"
            className="col-7"
          ></SCInput>
          <div className={`full-width flex flex-center mt-20`}>
            <SCButton
              loading={loading}
              text="Aceptar"
              disable={formState.isValid ? false : true}
            ></SCButton>
          </div>
        </SCForm>
      </div>
    </div>
  );
};

export default ChangePasswordPage;
