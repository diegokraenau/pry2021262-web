import { Login } from '../interfaces/login.interface';
import axios from 'axios';

export const login = async (login: Login) => {
  let token: any = null;
  await axios
    .post(`${process.env.REACT_APP_PROXY_HOST}/v1/auth/login`, {
      email: login.email,
      password: login.password,
    })
    .then((res) => {
      token = res.data.token;
    });

  return token;
};

export const sendEmail = async (email: string) => {
  let sent: any = null;
  await axios
    .post(`${process.env.REACT_APP_PROXY_HOST}/v1/auth/recover-password`, {
      email,
    })
    .then((res) => {
      sent = res.data;
    });

  return sent;
};
