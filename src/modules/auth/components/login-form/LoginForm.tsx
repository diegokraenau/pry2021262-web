import React, { useContext, useState } from 'react';
import style from './LoginForm.module.scss';
import { SCButton, SCForm, SCInput } from '@components';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { login } from '@modules/auth/commons/endpoints/auth.endpoint';
import { Login } from '@modules/auth/commons/interfaces/login.interface';
import { Auth } from '@classes';
import { MessageBusContext, AuthContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';
import { Link } from 'react-router-dom';
import { showWelcomeAlert } from '@alerts/welcome.alert';

const LoginForm = () => {
  /*Variables */
  const schema = yup.object().shape({
    email: yup
      .string()
      .email('Es necesario un correo')
      .required('Es necesario un correo')
      .min(10, 'Mínimo 10 caracteres')
      .max(100, 'Máximo 100 caracteres'),
    password: yup
      .string()
      .required('Es necesario una contraseña')
      .min(5, 'Mínimo 5 caracteres')
      .max(20, 'Máximo 20 caracteres'),
  });

  const { handleSubmit, register, formState, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const [loading, setLoading] = useState<boolean>(false);

  const { setMessageBus } = useContext(MessageBusContext);
  const { setUser, setCurrentTab } = useContext(AuthContext);

  /*Functions */

  const onSubmit = async (data: any) => {
    setLoading(true);
    const obj: Login = {
      email: data.email,
      password: data.password,
    };

    const token = await login(obj).finally(() => setLoading(false));

    if (token) {
      const auth = new Auth();
      auth.setToken(token);
      const decoded = auth.decodeToken();

      setUser(decoded);
      if (decoded.role === 'ADMINISTRATOR') {
        setMessageBus({
          channel: ChannelType.Global,
          topic: TopicType.Routing,
          data: {
            url: '/stats',
          },
        });
      } else {
        setCurrentTab(1);
        setMessageBus({
          channel: ChannelType.Global,
          topic: TopicType.Routing,
          data: {
            url: '/projects',
          },
        });
      }

      showWelcomeAlert(decoded.name, decoded.lastName, decoded.role);

      // history.push('/home');
    } else {
      setMessageBus({
        channel: ChannelType.Modals,
        topic: TopicType.Error,
        data: {
          title: 'Credenciales incorrectas',
          description: 'El email o contraseña son incorrectos',
        },
      });
    }
  };

  const openRegisterModal = () => {
    setMessageBus({
      channel: ChannelType.Modals,
      topic: TopicType.RegisterModal,
      data: null,
    });
  };

  const goToRecoverPassword = () => {
    setMessageBus({
      channel: ChannelType.Global,
      topic: TopicType.Routing,
      data: {
        url: '/recover-password',
      },
    });
  };

  return (
    <div className={`${style.container}`}>
      <h2 className={`${style['container-title']} text-center`}>
        Iniciar sesión
      </h2>
      <SCForm
        handleSubmit={handleSubmit}
        register={register}
        formState={formState}
        onSubmit={onSubmit}
        control={control}
      >
        <SCInput
          label="Email"
          name="email"
          placeholder="Ingrese el email"
          type="text"
        ></SCInput>
        <SCInput
          label="Contraseña"
          name="password"
          placeholder="Ingrese la contraseña"
          type="password"
          className="mb-40"
        ></SCInput>
        <div className="flex flex-space-between ">
          {/* <Link href="/recover-password">¿Recuperar contraseña?</a> */}
          <Link to="/recover-password">¿Olvistate tu contraseña?</Link>
        </div>
        <div className="full-width flex flex-center  mt-30">
          <SCButton
            loading={loading}
            disable={formState.isValid ? false : true}
            text="Iniciar sesión"
          ></SCButton>
        </div>
        <div className="seperator">
          <b>o</b>
        </div>
        <div className="full-width flex flex-center">
          <SCButton
            outline={true}
            text="Registrarse"
            onClick={() => openRegisterModal()}
            type="button"
          ></SCButton>
        </div>
      </SCForm>
    </div>
  );
};

export default LoginForm;
