import React from 'react';
import { SCButton, SCForm, SCInput, SCSelect } from '@components';
import { environment } from '@environment';
import styles from './HomePage.module.scss';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState } from 'react';
import { useContext } from 'react';
import { MessageBusContext } from '@contexts';
import { ChannelType, TopicType } from '@enums';

const HomePage = () => {
  //Trying form
  const [loading, setLoading] = useState(false);
  const { setMessageBus } = useContext(MessageBusContext);
  const onSubmit = (data: any) => {
    console.log(data);
    // setLoading(true);
    // setTimeout(() => {
    //   console.log(data);
    //   setLoading(false);
    // }, 5000);
  };

  const schema = yup.object().shape({
    firstName: yup.string().required('Es necesario el primer nombre'),
    lastName: yup.string().required('Es necesario el apellido'),
    country: yup.string().required('Es necesario el país'),
    perro: yup.string().required('Falta el perro'),
  });

  const { handleSubmit, register, formState, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const showHelloWord = (name: string) => {
    setMessageBus({
      channel: ChannelType.Modals,
      topic: TopicType.Evaluation,
      data: {
        title: '¿asfdasf?',
        perro: 'perrito',
      },
    });
  };

  const countryItems = [
    {
      id: 1,
      name: 'Perú',
    },
    {
      id: 2,
      name: 'Chile',
    },
    {
      id: 3,
      name: 'Argentina',
    },
  ];

  return (
    <div className={styles.container}>
      <h1 className={styles.container__title}>Home Page!</h1>

      <p>NAME: {environment.APP_NAME}</p>
      <p>URL: {environment.APP_URL}</p>

      <SCForm
        handleSubmit={handleSubmit}
        register={register}
        formState={formState}
        onSubmit={onSubmit}
        control={control}
      >
        <SCInput
          label="Firts name"
          name="firstName"
          placeholder="Primer nombre"
        ></SCInput>
        <SCInput
          label="Last name"
          name="lastName"
          placeholder="Apellido"
        ></SCInput>
        <SCInput
          label="Perro"
          name="perro"
          placeholder="Ingrese el perro"
        ></SCInput>
        <SCSelect
          placeholder="Seleccione un pasdfsdgsdgís"
          label="País"
          name="country"
          items={countryItems}
        ></SCSelect>
        <SCButton
          disable={formState.isValid ? false : true}
          loading={loading}
          text="Entrar"
          onClick={() => showHelloWord('Diego')}
        ></SCButton>
      </SCForm>
    </div>
  );
};

export default HomePage;
